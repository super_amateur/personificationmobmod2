package com.dodoneko.personificationmobmod.client.renderer.entity.layers;

import net.minecraft.client.renderer.entity.layers.LayerRenderer;

import com.dodoneko.personificationmobmod.client.renderer.entity.PMM_RenderWolf;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_LayerWolfCollar implements LayerRenderer<EntityWolf>
{
    private static final ResourceLocation WOLF_COLLAR = new ResourceLocation("textures/entity/wolf/wolf_collar.png");
    private final PMM_RenderWolf wolfRenderer;

    public PMM_LayerWolfCollar(PMM_RenderWolf pmm_RenderWolf)
    {
        this.wolfRenderer = pmm_RenderWolf;
    }

    @Override
	public void doRenderLayer(EntityWolf entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        if (entitylivingbaseIn.isTamed() && !entitylivingbaseIn.isInvisible())
        {
            this.wolfRenderer.bindTexture(WOLF_COLLAR);
            float[] afloat = EntitySheep.getDyeRgb(entitylivingbaseIn.getCollarColor());
            GlStateManager.color(afloat[0], afloat[1], afloat[2]);
            this.wolfRenderer.getMainModel().render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
        }
    }

    @Override
	public boolean shouldCombineTextures()
    {
        return true;
    }
}