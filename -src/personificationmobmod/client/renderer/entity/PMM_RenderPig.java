package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.client.model.PMM_ModelPig;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerSaddle;

import net.minecraft.client.model.ModelPig;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderPig extends RenderLiving<EntityPig>
{
	private static final ResourceLocation PIG_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
//  private static final ResourceLocation PIG_TEXTURES = new ResourceLocation("textures/entity/pig/pig.png");

    public PMM_RenderPig(RenderManager p_i47198_1_)
    {
        super(p_i47198_1_, new PMM_ModelPig(), 0.7F);
        this.addLayer(new PMM_LayerSaddle(this));
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityPig entity)
    {
        return PIG_TEXTURES;
    }
}