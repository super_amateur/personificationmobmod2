package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerCaveSpiderEyes;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSpider;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.monster.EntityCaveSpider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderCaveSpider extends PMM_RenderSpider<EntityCaveSpider>
{
    private static final ResourceLocation CAVE_SPIDER_TEXTURES = new ResourceLocation("textures/entity/spider/cave_spider-chan.png");

    public PMM_RenderCaveSpider(RenderManager renderManagerIn)
    {
        super(renderManagerIn);
        this.shadowSize *= 0.7F;
        this.layerRenderers.remove(this.layerRenderers.size()-1);
        this.addLayer(new PMM_LayerCaveSpiderEyes(this));
        
        ((PMM_ModelSpider)this.getMainModel()).boobHeight = 0.2f;
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityCaveSpider entitylivingbaseIn, float partialTickTime)
    {
        GlStateManager.scale(0.7F, 0.7F, 0.7F);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityCaveSpider entity)
    {
        return CAVE_SPIDER_TEXTURES;
    }
}