package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelZombie extends PMM_ModelBiped
{
    public PMM_ModelZombie()
    {
        this(0.0F, false);
    }

    public PMM_ModelZombie(float modelSize, boolean p_i1168_2_)
    {
        super(modelSize);
        boobHeight = 0.2F;
        modelScale = 1.0F;
    }
}