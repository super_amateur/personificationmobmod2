package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelCreeper extends PMM_ModelBiped
{

    public PMM_ModelCreeper(float modelSize)
    {
        super(modelSize, 0.0F, 64, 64);
        this.modelScale = 0.8F;
        this.boobHeight = 0.3F;
    }
}