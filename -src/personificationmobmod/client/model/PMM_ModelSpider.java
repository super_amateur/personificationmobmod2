package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.entity.Entity;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class PMM_ModelSpider extends PMM_ModelBiped {

    public PMM_ModelRenderer spiderRightArm2;
    public PMM_ModelRenderer spiderLeftArm2;
    public PMM_ModelRenderer spiderRightLeg2;
    public PMM_ModelRenderer spiderLeftLeg2;
    
	public PMM_ModelRenderer spiderLeftArmwear2;
	public PMM_ModelRenderer spiderRightArmwear2;
	public PMM_ModelRenderer spiderLeftLegwear2;
	public PMM_ModelRenderer spiderRightLegwear2;

    public PMM_ModelSpider(float modelSize)
    {
        super(modelSize, 0.0F, 64, 64);
        this.modelScale = 0.75F;
        this.boobHeight = 0.5F;

        this.spiderLeftArm2 = new PMM_ModelRenderer(this, 32, 48);
        this.spiderLeftArm2.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
        this.spiderLeftArm2.setRotationPoint(5.0F, 2.5F + 0, 0.0F);
        this.spiderRightArm2 = new PMM_ModelRenderer(this, 40, 16);
        this.spiderRightArm2.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
        this.spiderRightArm2.setRotationPoint(-5.0F, 2.5F + 0, 0.0F);
        this.spiderLeftArmwear2 = new PMM_ModelRenderer(this, 48, 48);
        this.spiderLeftArmwear2.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);
        this.spiderRightArmwear2 = new PMM_ModelRenderer(this, 40, 32);
        this.spiderRightArmwear2.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);

        this.spiderLeftLeg2 = new PMM_ModelRenderer(this, 16, 48);
        this.spiderLeftLeg2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
        this.spiderLeftLeg2.setRotationPoint(2.2F, 12.0F + 0, 0.0F);
        this.spiderRightLeg2 = new PMM_ModelRenderer(this, 0, 16);
        this.spiderRightLeg2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
        this.spiderRightLeg2.setRotationPoint(-2.2F, 12.0F + 0, 0.0F);
        this.spiderRightLegwear2 = new PMM_ModelRenderer(this, 0, 32);
        this.spiderRightLegwear2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
        this.spiderLeftLegwear2 = new PMM_ModelRenderer(this, 0, 48);
        this.spiderLeftLegwear2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
        
		this.bipedBody.addChild(this.spiderRightArm2);
		this.bipedBody.addChild(this.spiderLeftArm2);
		this.bipedBody.addChild(this.spiderRightLeg2);
		this.bipedBody.addChild(this.spiderLeftLeg2);

		this.spiderLeftArm2.addChild (this.spiderLeftArmwear2);
		this.spiderRightArm2.addChild(this.spiderRightArmwear2);
		this.spiderLeftLeg2.addChild (this.spiderLeftLegwear2);
		this.spiderRightLeg2.addChild(this.spiderRightLegwear2);
    }
    
    @Override
    public void setAddAnimations(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
	{
    	this.spiderLeftArm2.copy(this.bipedLeftArm);
    	this.spiderRightArm2.copy(this.bipedRightArm);
    	this.spiderLeftLeg2.copy(this.bipedLeftLeg);
    	this.spiderRightLeg2.copy(this.bipedRightLeg);

    	this.spiderLeftArm2.rotateAngleX  *= -1;
    	this.spiderRightArm2.rotateAngleX *= -1;
    	this.spiderLeftLeg2.rotateAngleX  *= -1;
    	this.spiderRightLeg2.rotateAngleX *= -1;

    	this.spiderLeftArm2.rotateAngleX  += 5 * PMM_Math.Deg2Rad;
    	this.spiderRightArm2.rotateAngleX += 5 * PMM_Math.Deg2Rad;
    	this.spiderLeftLeg2.rotateAngleX  += 10 * PMM_Math.Deg2Rad;
    	this.spiderRightLeg2.rotateAngleX += 10 * PMM_Math.Deg2Rad;

    	this.bipedLeftArm.rotateAngleX  -= 5 * PMM_Math.Deg2Rad;
    	this.bipedRightArm.rotateAngleX -= 5 * PMM_Math.Deg2Rad;
    	this.bipedLeftLeg.rotateAngleX  -= 10 * PMM_Math.Deg2Rad;
    	this.bipedRightLeg.rotateAngleX -= 10 * PMM_Math.Deg2Rad;
    	
    	this.spiderLeftArm2.rotateAngleZ  -= 20 * PMM_Math.Deg2Rad;
    	this.spiderRightArm2.rotateAngleZ += 20 * PMM_Math.Deg2Rad;
    	this.spiderLeftLeg2.rotateAngleZ  -= 12 * PMM_Math.Deg2Rad;
    	this.spiderRightLeg2.rotateAngleZ += 12 * PMM_Math.Deg2Rad;
    	
    	this.bipedLeftArm.rotateAngleZ  -= 5 * PMM_Math.Deg2Rad;
    	this.bipedRightArm.rotateAngleZ += 5 * PMM_Math.Deg2Rad;

    	this.spiderLeftArm2.rotationPointZ  += 2;
    	this.spiderRightArm2.rotationPointZ += 2;
    	this.spiderLeftLeg2.rotationPointZ  += 2;
    	this.spiderRightLeg2.rotationPointZ += 2;
    	
    	this.bipedLeftLeg.rotateAngleY    -= 10 * PMM_Math.Deg2Rad;
    	this.bipedRightLeg.rotateAngleY   += 10 * PMM_Math.Deg2Rad;
    	this.spiderLeftLeg2.rotateAngleY  -= 10 * PMM_Math.Deg2Rad;
    	this.spiderRightLeg2.rotateAngleY += 10 * PMM_Math.Deg2Rad;
	}
}
