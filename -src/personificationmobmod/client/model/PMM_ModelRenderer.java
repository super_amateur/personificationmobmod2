package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PMM_ModelRenderer extends ModelRenderer
{
    public float prevRotationPointX;
    public float prevRotationPointY;
    public float prevRotationPointZ;
    public float prevRotateAngleX;
    public float prevRotateAngleY;
    public float prevRotateAngleZ;

	public PMM_ModelRenderer(ModelBase model, String boxNameIn) {
		super(model, boxNameIn);
	}

    public PMM_ModelRenderer(ModelBase model)
    {
        this(model, (String)null);
    }

    public PMM_ModelRenderer(ModelBase model, int texOffX, int texOffY)
    {
        this(model);
        this.setTextureOffset(texOffX, texOffY);
    }
    

    @Override
	@SideOnly(Side.CLIENT)
    public void render(float scale)
    {
    	float rate = 0.5F;
        this.rotateAngleX = PMM_Math.lerpRotation(this.rotateAngleX, this.prevRotateAngleX, rate);
        this.rotateAngleY = PMM_Math.lerpRotation(this.rotateAngleY, this.prevRotateAngleY, rate);
        this.rotateAngleZ = PMM_Math.lerpRotation(this.rotateAngleZ, this.prevRotateAngleZ, rate);
        
    	super.render(scale);
    	
        this.prevRotationPointX = this.rotationPointX;
        this.prevRotationPointY = this.rotationPointY;
        this.prevRotationPointZ = this.rotationPointZ;
        this.prevRotateAngleX = this.rotateAngleX;
        this.prevRotateAngleY = this.rotateAngleY;
        this.prevRotateAngleZ = this.rotateAngleZ;
    }
	
	public void copy(PMM_ModelRenderer src)
	{
		this.isHidden = src.isHidden;
		this.rotateAngleX = src.rotateAngleX;
		this.rotateAngleY = src.rotateAngleY;
		this.rotateAngleZ = src.rotateAngleZ;
		this.rotationPointX = src.rotationPointX;
		this.rotationPointY = src.rotationPointY;
		this.rotationPointZ = src.rotationPointZ;
		this.showModel = src.showModel;
	}
	
	public void copyAll(PMM_ModelRenderer src)
	{
		this.isHidden = src.isHidden;
		this.rotateAngleX = src.rotateAngleX;
		this.rotateAngleY = src.rotateAngleY;
		this.rotateAngleZ = src.rotateAngleZ;
		this.rotationPointX = src.rotationPointX;
		this.rotationPointY = src.rotationPointY;
		this.rotationPointZ = src.rotationPointZ;
		this.prevRotateAngleX = src.prevRotateAngleX;
		this.prevRotateAngleY = src.prevRotateAngleY;
		this.prevRotateAngleZ = src.prevRotateAngleZ;
		this.prevRotationPointX = src.prevRotationPointX;
		this.prevRotationPointY = src.prevRotationPointY;
		this.prevRotationPointZ = src.prevRotationPointZ;
		this.showModel = src.showModel;
	}
	
	public PMM_ModelRenderer setTextureSize(int textureWidthIn, int textureHeightIn)
	{
		return (PMM_ModelRenderer)super.setTextureSize(textureWidthIn, textureHeightIn);
	}
	
	public void addChild(PMM_ModelRenderer renderer)
	{
		super.addChild((ModelRenderer)renderer);
	}
}
