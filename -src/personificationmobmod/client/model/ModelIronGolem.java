package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelIronGolem extends PMM_ModelBiped
{
    public ModelIronGolem()
    {
        this(0.0F);
    }

    public ModelIronGolem(float p_i1161_1_)
    {
        this(p_i1161_1_, -7.0F);
    }

    public ModelIronGolem(float p_i46362_1_, float p_i46362_2_)
    {
    	super();
    	this.modelScale = 1.8f;
    	this.boobHeight = 0.3f;
    }
}