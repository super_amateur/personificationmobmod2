package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSlime extends PMM_ModelBiped
{
    public PMM_ModelSlime(int p_i1157_1_)
    {
    	super();
    	this.modelScale = 1.0F - (float)p_i1157_1_ / 16F;
    	this.boobHeight = this.modelScale * 0.5F + 0.5F;
    	this.modelScale *= 2.5F;
    	this.isChild = true;
    }
}