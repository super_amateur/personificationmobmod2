package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelBat extends PMM_ModelBiped
{

    public PMM_ModelBat()
    {
    	super();
    	this.boobHeight = 0.15F;
    	this.modelScale = 1.0F;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setAddAnimations(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
        if (((EntityBat)entityIn).getIsBatHanging())
        {
            this.bipedHead.rotateAngleX = headPitch * 0.017453292F;
            this.bipedHead.rotateAngleY = (float)Math.PI - netHeadYaw * 0.017453292F;
            this.bipedHead.rotateAngleZ = (float)Math.PI;
//            this.bipedHead.setRotationPoint(0.0F, -2.0F, 0.0F);
//            this.bipedRightArm.setRotationPoint(-3.0F, 0.0F, 3.0F);
//            this.bipedLeftArm.setRotationPoint(3.0F, 0.0F, 3.0F);
            this.bipedBody.rotateAngleX = (float)Math.PI;
            this.bipedRightArm.rotateAngleX = -0.15707964F;
            this.bipedRightArm.rotateAngleY = -((float)Math.PI * 2F / 5F);
//            this.bipedOuterRightWing.rotateAngleY = -1.7278761F;
            this.bipedLeftArm.rotateAngleX = this.bipedRightArm.rotateAngleX;
            this.bipedLeftArm.rotateAngleY = -this.bipedRightArm.rotateAngleY;
//            this.bipedOuterLeftWing.rotateAngleY = -this.bipedOuterRightWing.rotateAngleY;
        }
        else
        {
            this.bipedHead.rotateAngleX = headPitch * 0.017453292F;
            this.bipedHead.rotateAngleY = netHeadYaw * 0.017453292F;
            this.bipedHead.rotateAngleZ = 0.0F;
//            this.bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
//            this.bipedRightArm.setRotationPoint(0.0F, 0.0F, 0.0F);
//            this.bipedLeftArm.setRotationPoint(0.0F, 0.0F, 0.0F);
            this.bipedBody.rotateAngleX = ((float)Math.PI / 4F) + MathHelper.cos(ageInTicks * 0.1F) * 0.15F;
            this.bipedBody.rotateAngleY = 0.0F;
            
            float tcos = 1;//MathHelper.cos(ageInTicks * 1.3F * 0.1F) * 0.5F + 0.5F;
            this.bipedRightArm.rotateAngleZ = PMM_Math.lerp(0F, 45F, tcos) * PMM_Math.Deg2Rad;
            this.bipedRightArm.rotateAngleX = PMM_Math.lerp(-75F, -135F, tcos) * PMM_Math.Deg2Rad;
            this.bipedRightArm.rotateAngleY = PMM_Math.lerp(0F, 120F, tcos) * PMM_Math.Deg2Rad;
            
        }
        
    	this.isChild = true;
    }
}