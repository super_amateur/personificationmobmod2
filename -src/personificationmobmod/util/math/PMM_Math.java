package com.dodoneko.personificationmobmod.util.math;

public class PMM_Math {
	
	public static final float Deg2Rad = 0.017453292F;
	
	public static float lerpRotation(float a, float b, float rate) {
		if (b-a > (float)Math.PI) b -= (float)Math.PI;
		if (b-a < -(float)Math.PI) b += (float)Math.PI;
		return a + (b - a) * rate;
	}
	
	public static float lerp(float a, float b, float rate) {
		return a + (b - a) * rate;
	}
	
	public static float clamp(float value, float min, float max)
	{
		return Math.min(max, Math.max(min, value));
	}
	public static int clamp(int value, int min, int max)
	{
		return Math.min(max, Math.max(min, value));
	}
	
	public static float sqrt(float value)
	{
		return (float)Math.sqrt(value);
	}
}