package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelBiped extends ModelBiped
{

    public PMM_ModelRenderer bipedHead;
    public PMM_ModelRenderer bipedBody;
    public PMM_ModelRenderer bipedRightArm;
    public PMM_ModelRenderer bipedLeftArm;
    public PMM_ModelRenderer bipedRightLeg;
    public PMM_ModelRenderer bipedLeftLeg;
    
	public PMM_ModelRenderer bipedLeftArmwear;
	public PMM_ModelRenderer bipedRightArmwear;
	public PMM_ModelRenderer bipedLeftLegwear;
	public PMM_ModelRenderer bipedRightLegwear;
	public PMM_ModelRenderer bipedBodywear;
    public PMM_ModelRenderer bipedHeadwear;

	public PMM_ModelRenderer bipedUpperBoob;
	public PMM_ModelRenderer bipedLowerBoob;
	public PMM_ModelRenderer bipedUpperBoobwear;
	public PMM_ModelRenderer bipedLowerBoobwear;
	public PMM_ModelRenderer bipedRightEyelid;
	public PMM_ModelRenderer bipedLeftEyelid;
	public PMM_ModelRenderer bipedAhoge;

	public boolean ahogeSwing = true;
	public boolean doTwinkles = true;
	public boolean doWalkBounding = true;
	public float modelScale = 1.0F;
	/// 0..1
	public float boobHeight;

	protected long lastFrameTime;
	protected float twinklesTime;
	protected boolean twinkledNow;

	public PMM_ModelBiped()
	{
		this(0.0F);
	}

	public PMM_ModelBiped(float modelSize)
	{
		this(modelSize, 0.0F, 64, 64);
	}

	public PMM_ModelBiped(float modelSize, float p_i1149_2_, int textureWidthIn, int textureHeightIn)
	{
		super(modelSize, p_i1149_2_, textureWidthIn, textureHeightIn);
		
        this.bipedHead = new PMM_ModelRenderer(this, 0, 0);
        this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, modelSize);
        this.bipedHead.setRotationPoint(0.0F, 0.0F + p_i1149_2_, 0.0F);
        this.bipedHeadwear = new PMM_ModelRenderer(this, 32, 0);
        this.bipedHeadwear.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, modelSize + 0.5F);
        this.bipedHeadwear.setRotationPoint(0.0F, 0.0F + p_i1149_2_, 0.0F);
        this.bipedBody = new PMM_ModelRenderer(this, 16, 16);
        this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, modelSize);
        this.bipedBody.setRotationPoint(0.0F, 0.0F + p_i1149_2_, 0.0F);

		this.bipedLeftArm = new PMM_ModelRenderer(this, 32, 48);
		this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
		this.bipedLeftArm.setRotationPoint(5.0F, 2.5F + p_i1149_2_, 0.0F);
		this.bipedRightArm = new PMM_ModelRenderer(this, 40, 16);
		this.bipedRightArm.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
		this.bipedRightArm.setRotationPoint(-5.0F, 2.5F + p_i1149_2_, 0.0F);
		this.bipedLeftArmwear = new PMM_ModelRenderer(this, 48, 48);
		this.bipedLeftArmwear.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);
		this.bipedRightArmwear = new PMM_ModelRenderer(this, 40, 32);
		this.bipedRightArmwear.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);

		this.bipedLeftLeg = new PMM_ModelRenderer(this, 16, 48);
		this.bipedLeftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
		this.bipedLeftLeg.setRotationPoint(2.2F, 12.0F + p_i1149_2_, 0.0F);
		this.bipedRightLeg = new PMM_ModelRenderer(this, 0, 16);
		this.bipedRightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
		this.bipedRightLeg.setRotationPoint(-2.2F, 12.0F + p_i1149_2_, 0.0F);
		this.bipedRightLegwear = new PMM_ModelRenderer(this, 0, 32);
		this.bipedRightLegwear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
		this.bipedLeftLegwear = new PMM_ModelRenderer(this, 0, 48);
		this.bipedLeftLegwear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);

		this.bipedBodywear = new PMM_ModelRenderer(this, 16, 32);
		this.bipedBodywear.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, modelSize + 0.25F);

		this.bipedUpperBoob = new PMM_ModelRenderer(this, 16, 21);
		this.bipedUpperBoob.addBox(-4.0F, 0.0F, -4.0F, 8, 4, 4, modelSize - 0.01F);
		this.bipedUpperBoob.setRotationPoint(0.0F, 0.0F, -2.0F);
		this.bipedLowerBoob = new PMM_ModelRenderer(this, 16, 25);
		this.bipedLowerBoob.addBox(-4.0F, 0.0F, -4.0F, 8, 3, 4, modelSize - 0.02F);
		this.bipedLowerBoob.setRotationPoint(0.0F, 0.0F, -4.0F);
		this.bipedUpperBoobwear = new PMM_ModelRenderer(this, 16, 36);
		this.bipedUpperBoobwear.addBox(-4.0F, 0.152F, -4.125F, 8, 4, 4, modelSize + 0.24F);
		this.bipedLowerBoobwear = new PMM_ModelRenderer(this, 16, 40);
		this.bipedLowerBoobwear.addBox(-4.0F, 0.125F, -4.125F, 8, 3, 4, modelSize + 0.23F);

		this.bipedLeftEyelid = new PMM_ModelRenderer(this, 12, 16);
		this.bipedLeftEyelid.addBox(-3.0F, -4.0F, 2.1F, 2, 2, 2, modelSize);
		this.bipedLeftEyelid.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedLeftEyelid.rotateAngleY = 3.141592653F;
		this.bipedLeftEyelid.prevRotateAngleY = 3.141592653F;
		this.bipedRightEyelid = new PMM_ModelRenderer(this, 12, 16);
		this.bipedRightEyelid.addBox(-3.0F, -4.0F, -4.1F, 2, 2, 2, modelSize);
		this.bipedRightEyelid.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedAhoge = new PMM_ModelRenderer(this, 24, 0);
		this.bipedAhoge.addBox(-3.5F, -3.0F, -1.0F, 7, 3, 1, modelSize);
		this.bipedAhoge.setRotationPoint(0.0F, -8.0F, 0.0F);
		
		this.bipedBody.addChild(this.bipedRightArm);
		this.bipedBody.addChild(this.bipedLeftArm);
		this.bipedBody.addChild(this.bipedRightLeg);
		this.bipedBody.addChild(this.bipedLeftLeg);
		this.bipedBody.addChild(this.bipedUpperBoob);
		this.bipedUpperBoob.addChild(this.bipedLowerBoob);
		
		this.bipedHead.addChild(this.bipedHeadwear);
		this.bipedHead.addChild(this.bipedAhoge);
		this.bipedHead.addChild(this.bipedLeftEyelid);
		this.bipedHead.addChild(this.bipedRightEyelid);
		this.bipedBody.addChild(this.bipedBodywear);
		this.bipedLeftArm.addChild (this.bipedLeftArmwear);
		this.bipedRightArm.addChild(this.bipedRightArmwear);
		this.bipedLeftLeg.addChild (this.bipedLeftLegwear);
		this.bipedRightLeg.addChild(this.bipedRightLegwear);
		this.bipedUpperBoob.addChild(this.bipedUpperBoobwear);
		this.bipedLowerBoob.addChild(this.bipedLowerBoobwear);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		limbSwing *= 1.2F / modelScale;
		if (doWalkBounding)
		{
			GlStateManager.translate(0.0F, MathHelper.abs(MathHelper.cos(limbSwing * 1.3314F)) * limbSwingAmount * this.modelScale * 0.0625F * 4 - 0.0625F * 2 * limbSwingAmount * (this.isChild? 0.5F: 1F), 0.0F);
		}
		scale *= this.modelScale;
		this.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale, entityIn);
		GlStateManager.pushMatrix();

		if (this.isChild)
		{
			this.bipedHeadwear.isHidden = true;
			this.bipedLowerBoob.isHidden = true;
			this.bipedUpperBoob.isHidden = true;
			this.bipedLowerBoobwear.isHidden = true;
			this.bipedUpperBoobwear.isHidden = true;
			
			GlStateManager.translate(0.0F, 1.5F * (1F-this.modelScale), 0.0F);
			GlStateManager.scale(0.75F, 0.75F, 0.75F);
			GlStateManager.translate(0.0F, 16.0F * scale, 0.0F);
			this.bipedHead.render(scale);
			GlStateManager.popMatrix();
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.0F, 1.5F * (1F-this.modelScale), 0.0F);
			GlStateManager.scale(0.5F, 0.5F, 0.5F);
			GlStateManager.translate(0.0F, 24.0F * scale, 0.0F);
			this.bipedBody.render(scale);
		}
		else
		{
			GlStateManager.translate(0.0F, -1.5F * this.modelScale + 1.5F, 0.0F);
			if (entityIn.isSneaking())
			{
				GlStateManager.translate(0.0F, 0.2F * modelScale, 0.0F);
			}

			this.bipedHeadwear.isHidden = false;
			if (boobHeight > 0.000012)
			{
				if (boobHeight < 0.999996F)
				{
					
					this.bipedLowerBoob.isHidden = false;
					this.bipedUpperBoob.isHidden = false;
					this.bipedLowerBoobwear.isHidden = false;
					this.bipedUpperBoobwear.isHidden = false;
				}
				else {
					this.bipedLowerBoob.isHidden = true;
					this.bipedUpperBoob.isHidden = false;
					this.bipedLowerBoobwear.isHidden = true;
					this.bipedUpperBoobwear.isHidden = false;
				}
			}
			else {
				this.bipedLowerBoob.isHidden = true;
				this.bipedUpperBoob.isHidden = true;
				this.bipedLowerBoobwear.isHidden = true;
				this.bipedUpperBoobwear.isHidden = true;
			}
			
			this.bipedHead.render(scale);
			this.bipedBody.render(scale);
		}
		GlStateManager.popMatrix();
	}

	/**
	 * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
	 * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
	 * "far" arms and legs can swing at most.
	 */
	@Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
	{
		// → RenderのdoRender()でentity情報を入れるのでは？
		// 頭の回転
		this.setHeadYawAndPitch(netHeadYaw, headPitch);
		
		// 何もしてないときのモーション
		this.setStayAnimations(ageInTicks);

		// 歩いてるときのモーション
        if (limbSwingAmount > 0.0F)
        {
        	this.setWalkingAnimations(limbSwing, limbSwingAmount);
        }

		// 乗ってる時のモーション
		if (this.isRiding)
		{
			this.setRidingAnimations();
		}

		// 手に何か持ってるとき
		this.setHasAnythingAnimations(this.leftArmPose, this.rightArmPose);

		// 叩く時のモーション
		if (this.swingProgress > 0.0F)
		{
			this.setSwingProgressAnimations(((EntityLiving)entityIn));
		}

		// 弓持ってる時のモーション
		if (this.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW || this.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW)
		{
			this.setHasBowAnimations(this.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW, this.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW);
		}

		// ダメージ時のモーション
//		if (((EntityLiving)entityIn).hurtTime > 1)
//		{
//			this.setDamagedAnimations(((EntityLiving)entityIn), limbSwing, limbSwingAmount);
//		}
		// 空中にいるの時のモーション
		else if (MathHelper.abs((float)((EntityLiving)entityIn).motionY) > 0.001F && !entityIn.onGround)
		{
			// 水中にいるとき
			if (((EntityLiving)entityIn).isInWater())
			{
				this.setSwimmingAnimations(((EntityLiving)entityIn), ageInTicks);
			} else 
			{
				this.setJumpAnimations(((EntityLiving)entityIn));
			}
		}

		this.setAddAnimations(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor, entityIn);

		this.setBoobsAnimations(limbSwing, limbSwingAmount);
		this.setAhogeAnimations(limbSwing, limbSwingAmount);
		this.setTwinkleAnimations(netHeadYaw * headPitch * limbSwing);
	}


	
	/** 頭の回転 */
	protected void setHeadYawAndPitch(float netHeadYaw, float headPitch)
	{
		this.bipedHead.rotateAngleY = netHeadYaw * PMM_Math.Deg2Rad;
		this.bipedHead.rotateAngleX = headPitch * PMM_Math.Deg2Rad;
	}

	/** あほげのアニメーション */
	protected void setAhogeAnimations(float limbSwing, float limbSwingAmount)
	{
		this.bipedAhoge.rotateAngleY = this.bipedHead.rotateAngleY;
		this.bipedAhoge.rotateAngleX = lerpRotation(this.bipedAhoge.rotateAngleX, this.bipedHead.rotateAngleX * 1.5f, 0.1F);
		if (ahogeSwing)
			this.bipedAhoge.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F * 2F + (float)Math.PI * limbSwingAmount) * 0.2F * limbSwingAmount;
	}

	/** おっぱい部分のアニメーション */
	protected void setBoobsAnimations(float limbSwing, float limbSwingAmount)
	{
		this.bipedUpperBoob.rotationPointY = this.boobHeight;
		this.bipedUpperBoob.rotationPointY += (this.boobHeight - MathHelper.abs(MathHelper.cos(limbSwing * 1.3314F + 0.5F)) * this.boobHeight*2) * limbSwingAmount;

		this.bipedUpperBoob.rotateAngleX = -(float)Math.asin((double)this.boobHeight * 0.707106781F) + 1.570796326F;
		this.bipedUpperBoob.rotateAngleX += (MathHelper.abs(MathHelper.cos(limbSwing * 1.3314F + 0.75F)) * 30 * this.boobHeight * PMM_Math.Deg2Rad) * limbSwingAmount;
		float f = this.boobHeight;
		if (this.boobHeight > 0.5F) f = 1.0F - f;
		this.bipedLowerBoob.rotateAngleX = -(this.bipedUpperBoob.rotateAngleX - 1.570796326F) * (2 + f);
	}

	/** まばたき */
	protected void setTwinkleAnimations(float hash)
	{
		Minecraft.getMinecraft();
		this.twinklesTime -= Minecraft.getSystemTime()/1000F - lastFrameTime/1000F; 
		Minecraft.getMinecraft();
		this.lastFrameTime = Minecraft.getSystemTime();
		hash = (hash/100) %1;
		if (this.twinkledNow)
		{
			if (this.twinklesTime < 0.0F)
			{
				this.twinkledNow = false;
				this.twinklesTime = (float)Math.random() * 8 + hash;
			}
		}
		else
		{
			if (this.twinklesTime < 0.0F)
			{
				this.twinkledNow = true;
				this.twinklesTime = (float)Math.random() + hash;
			}
		}

		this.bipedLeftEyelid.isHidden  = !this.twinkledNow;
		this.bipedRightEyelid.isHidden = !this.twinkledNow;

	}
	
	/** 何もしてないときのモーション */
	protected void setStayAnimations(float ageInTicks)
	{
		this.bipedRightArm.rotateAngleZ = MathHelper.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
        this.bipedLeftArm.rotateAngleZ  = -MathHelper.cos(ageInTicks * 0.09F) * 0.05F - 0.05F;
        this.bipedRightArm.rotateAngleX = MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
        this.bipedLeftArm.rotateAngleX  = -MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
        this.bipedRightArm.rotateAngleY = 0.0F;
        this.bipedLeftArm.rotateAngleY  = 0.0F;
        
        this.bipedRightLeg.rotateAngleZ = 0.0F;
        this.bipedLeftLeg.rotateAngleZ  = 0.0F;
        this.bipedRightLeg.rotateAngleX = 0.0F;
        this.bipedLeftLeg.rotateAngleX  = 0.0F;
        this.bipedRightLeg.rotateAngleY = 0.0F;
        this.bipedLeftLeg.rotateAngleY  = 0.0F;
	}
	
	/** 水中にいるときのモーション */
	protected void setSwimmingAnimations(EntityLiving entityIn, float ageInTicks)
	{
		this.bipedRightLeg.rotateAngleY =  0.15F;
		this.bipedLeftLeg.rotateAngleY  = -0.15F;
		this.bipedRightLeg.rotateAngleX = MathHelper.cos(ageInTicks * 0.6F) * 1.4F;
		this.bipedLeftLeg.rotateAngleX  = MathHelper.cos(ageInTicks * 0.6F + (float)Math.PI) * 1.4F;
		this.bipedRightLeg.rotateAngleZ =  0.3F;
		this.bipedLeftLeg.rotateAngleZ  = -0.3F;
	}

	/** onGroundじゃない時のモーション */
	protected void setJumpAnimations(EntityLiving entityIn)
	{
		float fallingSpeed = MathHelper.clamp((float)entityIn.motionY, -1.0F, 1.0F);
		this.bipedLeftArm.rotateAngleZ  = -90 * PMM_Math.Deg2Rad + fallingSpeed;
		this.bipedRightArm.rotateAngleZ =  90 * PMM_Math.Deg2Rad - fallingSpeed;
		this.bipedLeftLeg.rotateAngleY = -fallingSpeed / 3F;
		this.bipedLeftLeg.rotateAngleY =  fallingSpeed / 3F;
		this.bipedLeftLeg.rotateAngleX  = fallingSpeed;
		this.bipedRightLeg.rotateAngleX = fallingSpeed;
	}

	/** ダメージ受けた時のアニメーション  */
	protected void setDamagedAnimations(EntityLiving entityIn, float limbSwing, float limbSwingAmount)
	{
		this.setWalkingAnimations(limbSwing, limbSwingAmount);
		this.bipedHead.rotateAngleX = 15F * PMM_Math.Deg2Rad;
		this.bipedLeftLeg.rotateAngleX  -= 20F * PMM_Math.Deg2Rad;
		this.bipedRightLeg.rotateAngleX -= 20F * PMM_Math.Deg2Rad;
		this.bipedLeftArm.rotateAngleZ  -= 80F * PMM_Math.Deg2Rad;
		this.bipedRightArm.rotateAngleZ += 80F * PMM_Math.Deg2Rad;
		this.bipedLeftArm.rotateAngleY -= 80F * PMM_Math.Deg2Rad;
		this.bipedRightArm.rotateAngleY += 80F * PMM_Math.Deg2Rad;
	}

	/** 弓矢持ってる時のアニメーション */
	protected void setHasBowAnimations(boolean hasBowRight, boolean hasBowLeft)
	{
		this.bipedRightArm.rotateAngleY = -0.1F + this.bipedHead.rotateAngleY - (hasBowLeft? 0.4F: 0.0F);
		this.bipedLeftArm.rotateAngleY = 0.1F + this.bipedHead.rotateAngleY + (hasBowRight? 0.4F: 0.0F);
		this.bipedRightArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
		this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
	}
	
	/** 歩いてる時のモーション */
	protected void setWalkingAnimations(float limbSwing, float limbSwingAmount)
	{
		this.bipedRightArm.rotateAngleY = 0.5F * limbSwingAmount;
		this.bipedLeftArm.rotateAngleY  = -0.5F * limbSwingAmount;
		this.bipedRightArm.rotateAngleX = MathHelper.cos(limbSwing * 1.3324F + (float)Math.PI) * 2.0F * limbSwingAmount * 0.5F;
		this.bipedLeftArm.rotateAngleX  = MathHelper.cos(limbSwing * 1.3324F) * 2.0F * limbSwingAmount * 0.5F;
		this.bipedRightArm.rotateAngleZ = 0.5F * limbSwingAmount;
		this.bipedLeftArm.rotateAngleZ  = -0.5F * limbSwingAmount;
		this.bipedRightLeg.rotateAngleY =  0.15F * limbSwingAmount;
		this.bipedLeftLeg.rotateAngleY  = -0.15F * limbSwingAmount;
		this.bipedRightLeg.rotateAngleX = MathHelper.cos(limbSwing * 1.3324F) * 1.4F * limbSwingAmount;
		this.bipedLeftLeg.rotateAngleX  = MathHelper.cos(limbSwing * 1.3324F + (float)Math.PI) * 1.4F * limbSwingAmount;
		this.bipedRightLeg.rotateAngleZ =  0.15F * limbSwingAmount;
		this.bipedLeftLeg.rotateAngleZ  = -0.15F * limbSwingAmount;
	}
	
	/** 何かに乗ってる時のモーション */
	protected void setRidingAnimations()
	{
		this.bipedRightArm.rotateAngleX = -((float)Math.PI / 5F);
		this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 5F);
		this.bipedRightLeg.rotateAngleX = -1.4137167F;
		this.bipedRightLeg.rotateAngleY = ((float)Math.PI / 10F);
		this.bipedRightLeg.rotateAngleZ = 0.07853982F;
		this.bipedLeftLeg.rotateAngleX = -1.4137167F;
		this.bipedLeftLeg.rotateAngleY = -((float)Math.PI / 10F);
		this.bipedLeftLeg.rotateAngleZ = -0.07853982F;
	}
	
	/** 手に何か持ってる時のアニメーション */
	protected void setHasAnythingAnimations(ArmPose leftArmPose, ArmPose rightArmPose)
	{
		switch (leftArmPose)
		{
		case BLOCK:
			this.bipedLeftArm.rotateAngleX = this.bipedLeftArm.rotateAngleX * 0.5F - 0.9424779F;
			this.bipedLeftArm.rotateAngleY = 0.5235988F;
			break;
		case ITEM:
			this.bipedLeftArm.rotateAngleX = this.bipedLeftArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F);
			this.bipedLeftArm.rotateAngleY = 0.0F;
			break;
		default:
			break;
		}

		switch (rightArmPose)
		{
		case BLOCK:
			this.bipedRightArm.rotateAngleX = this.bipedRightArm.rotateAngleX * 0.5F - 0.9424779F;
			this.bipedRightArm.rotateAngleY = -0.5235988F;
			break;
		case ITEM:
			this.bipedRightArm.rotateAngleX = this.bipedRightArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F);
			this.bipedRightArm.rotateAngleY = 0.0F;
			break;
		default:
			break;
		}
	}

	/** 叩くときのモーション*/
	protected void setSwingProgressAnimations(EntityLiving entityIn)
	{
		EnumHandSide enumhandside = this.getMainHand(entityIn);
		PMM_ModelRenderer modelrenderer = (PMM_ModelRenderer) this.getArmForSide(enumhandside);
		float f1 = this.swingProgress;
		this.bipedBody.rotateAngleY = MathHelper.sin(MathHelper.sqrt(f1) * ((float)Math.PI * 2F)) * 0.2F;

		if (enumhandside == EnumHandSide.LEFT)
		{
			this.bipedBody.rotateAngleY *= -1.0F;
		}

		this.bipedRightArm.rotationPointZ = MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedRightArm.rotationPointX = -MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedLeftArm.rotationPointZ = -MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedLeftArm.rotationPointX = MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedRightArm.rotateAngleY = this.bipedBody.rotateAngleY;
		this.bipedLeftArm.rotateAngleY = this.bipedBody.rotateAngleY;
		this.bipedLeftArm.rotateAngleX += this.bipedBody.rotateAngleY;
		f1 = 1.0F - this.swingProgress;
		f1 = f1 * f1;
		f1 = f1 * f1;
		f1 = 1.0F - f1;
		float f2 = MathHelper.sin(f1 * (float)Math.PI);
		float f3 = MathHelper.sin(this.swingProgress * (float)Math.PI) * -(this.bipedHead.rotateAngleX - 0.7F) * 0.75F;
		modelrenderer.rotateAngleX = (float)(modelrenderer.rotateAngleX - (f2 * 1.2D + f3));
		modelrenderer.rotateAngleY += this.bipedBody.rotateAngleY * 2.0F;
		modelrenderer.rotateAngleZ += MathHelper.sin(this.swingProgress * (float)Math.PI) * -0.4F;
	}

	protected void setAddAnimations(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
	{
	}




	protected float lerpRotation(float a, float b, float rate) {
		if (b-a > (float)Math.PI) b -= (float)Math.PI;
		if (b-a < -(float)Math.PI) b += (float)Math.PI;
		return a + (b - a) * rate;
	}

	@Override
	public void setModelAttributes(ModelBase model)
	{
		super.setModelAttributes(model);

		if (model instanceof PMM_ModelBiped)
		{
			PMM_ModelBiped modelbiped = (PMM_ModelBiped)model;
			this.isSneak = modelbiped.isSneak;
		}
	}
}