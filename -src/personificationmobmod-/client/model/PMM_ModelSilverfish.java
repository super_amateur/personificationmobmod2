package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSilverfish extends PMM_ModelBiped
{
	// TODO: うつぶせで両腕も上げてﾋﾞﾀﾝﾋﾞﾀﾝって感じに
    public PMM_ModelSilverfish()
    {
    	super();
    	this.modelScale = 0.5f;
    }
}