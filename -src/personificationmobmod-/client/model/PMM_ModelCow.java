package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelCow extends PMM_ModelBiped
{
    public PMM_ModelCow()
    {
        super();
        this.boobHeight = 1.0f;
        this.modelScale = 0.75F;
    }
}