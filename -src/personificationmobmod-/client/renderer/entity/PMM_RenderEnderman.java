package com.dodoneko.personificationmobmod.client.renderer.entity;

import java.util.Random;

//import com.dodoneko.personificationmobmod.client.model.PMM_ModelBiped;
 import com.dodoneko.personificationmobmod.client.model.PMM_ModelEnderman;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerEndermanEyes;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerHeldBlock;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderEnderman extends RenderLiving<EntityEnderman>
{
    private static final ResourceLocation ENDERMAN_TEXTURES = new ResourceLocation("textures/entity/enderman/enderman-chan.png");
    private final Random rnd = new Random();

    public PMM_RenderEnderman(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelEnderman(0.0F), 0.5F);
        this.addLayer(new PMM_LayerEndermanEyes(this));
        this.addLayer(new PMM_LayerHeldBlock(this));
    }

    @Override
	public PMM_ModelEnderman getMainModel()
    {
        return (PMM_ModelEnderman)super.getMainModel();
    }

    /**
     * Renders the desired {@code T} type Entity.
     */
    @Override
	public void doRender(EntityEnderman entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
        IBlockState iblockstate = entity.getHeldBlockState();
        PMM_ModelEnderman modelenderman = this.getMainModel();
        modelenderman.isCarrying = iblockstate != null;
        modelenderman.isAttacking = entity.isScreaming();

        if (entity.isScreaming())
        {
            double d0 = 0.02D;
            x += this.rnd.nextGaussian() * 0.02D;
            z += this.rnd.nextGaussian() * 0.02D;
        }

        super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityEnderman entity)
    {
        return ENDERMAN_TEXTURES;
    }
}