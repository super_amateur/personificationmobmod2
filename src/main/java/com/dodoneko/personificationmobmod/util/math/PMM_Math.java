package com.dodoneko.personificationmobmod.util.math;

public class PMM_Math
{
	public static final float Deg2Rad = 0.017453292F;
	public static final float Rad2Deg = 57.295779513F;
	public static float Rad(float deg) {return deg * Deg2Rad;}
	public static float Deg(float rad) {return rad * Rad2Deg;}
	
	public static final float PI = 3.141592653F;
	public static final float PI_Half = 1.570796326F;
	public static final float PI_Double = 6.283185307F;
	
	/**
	 * @return aからrate分bに偏った値を返す。<br>360度(=0度)を通る方が近い場合は360度を通る方でのrateになる。
	 */
	public static float lerpRotation(float a, float b, float rate) {
		if (b-a > PI) b -= PI;
		if (b-a < -PI) b += PI;
		return a + (b - a) * rate;
	}
	
	/**
	 * @return aからrate分bに偏った値を返す。
	 */
	public static float lerp(float a, float b, float rate) {
		return a + (b - a) * rate;
	}
	
	/**
	 * @return valueをmin以上max以下に強制した値を返す。<br>max＜minの場合はNaNを返す
	 */
	public static float clamp(float value, float min, float max)
	{
		if (max < min) return Float.NaN;
		return Math.min(max, Math.max(min, value));
	}
	/**
	 * @return valueをmin以上max未満に強制した値を返す。<br>max-1＜minの場合はMIN_VALUEを返す
	 */
	public static int clamp(int value, int min, int max)
	{
		if (max-1 < min) return Integer.MIN_VALUE;
		return Math.min(max-1, Math.max(min, value));
	}
	
	public static float sqrt(float value)
	{
		return (float)Math.sqrt(value);
	}
	
	/**
	 * @param time サイン波を生成する時間軸に当たる変数を入れる。
	 * @param speed (1/サイン波の振動数)を決める。
	 * @param timeLag サイン波の始点を指定する(0..1)。 (省略可)
	 * @return　0..1 に正規化されたサイン波が返ってくる。
	 */
	public static float normalizedSinWave(float time, float speed, float timeLag)
	{
		return (float)Math.cos((double)time * (double)speed + (double)timeLag * (double)PI_Double) * 0.5F + 0.5F;
	}
	/**
	 * @param time サイン波を生成する時間軸に当たる変数を入れる。
	 * @param speed (1/サイン波の振動数)を決める。
	 * @return　0..1 に正規化されたサイン波が返ってくる。
	 */
	public static float normalizedSinWave(float time, float speed)
	{
		return normalizedSinWave(time, speed, 0.0F);
	}
}