package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelOcelot extends PMM_ModelBiped
{

    public PMM_ModelOcelot()
    {
    	super();
    }
}