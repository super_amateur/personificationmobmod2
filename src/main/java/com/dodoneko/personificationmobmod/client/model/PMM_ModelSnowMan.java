package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSnowMan extends PMM_ModelBiped
{
	//TODO: かぼちゃ頭の代わりに、帽子を脱がす。
    public PMM_ModelSnowMan()
    {
    	super();
    	this.boobHeight = 0.25f;
    }
}