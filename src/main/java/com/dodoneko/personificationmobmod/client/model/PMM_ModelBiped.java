package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelBiped extends ModelBiped
{

    public PMM_ModelRenderer bipedHead;
    public PMM_ModelRenderer bipedBody;
    public PMM_ModelRenderer bipedRightArm;
    public PMM_ModelRenderer bipedLeftArm;
    public PMM_ModelRenderer bipedRightLeg;
    public PMM_ModelRenderer bipedLeftLeg;
    
	public PMM_ModelRenderer bipedLeftArmwear;
	public PMM_ModelRenderer bipedRightArmwear;
	public PMM_ModelRenderer bipedLeftLegwear;
	public PMM_ModelRenderer bipedRightLegwear;
	public PMM_ModelRenderer bipedBodywear;
    public PMM_ModelRenderer bipedHeadwear;

	public PMM_ModelRenderer bipedUpperBoob;
	public PMM_ModelRenderer bipedLowerBoob;
	public PMM_ModelRenderer bipedUpperBoobwear;
	public PMM_ModelRenderer bipedLowerBoobwear;
	public PMM_ModelRenderer bipedRightEyelid;
	public PMM_ModelRenderer bipedLeftEyelid;
	public PMM_ModelRenderer bipedAhoge;

	public boolean ahogeSwing = true;
	public boolean doTwinkles = true;
	public boolean doWalkBounding = true;
	public float modelScale = 1.0F;
	public float walkSpeed = 1.0F;
	/// 0..1
	public float boobHeight;

	protected Map<Integer, float> twinklesTimes = new HashMap();
	protected boolean twinkledNow;
    
    // TODO: Remove these parameter and processing (in Renderer) because motion error from ease move. It had debugged.
	// using in animations
	public float limbSwing;
	public float limbSwingAmount;
	public float ageInTicks;
	public float netHeadYaw;
	public float headPitch;
	public float scaleFactor;
	public Entity entityIn;
	
	public int entityId;
	public double entityMotionY;
	public boolean entityOnGround;
	public boolean entityIsInWater;
	public int entityHurtTime;

	public PMM_ModelBiped()
	{
		this(0.0F);
	}

	public PMM_ModelBiped(float modelSize)
	{
		this(modelSize, 0.0F, 64, 64);
	}

	public PMM_ModelBiped(float modelSize, float p_i1149_2_, int textureWidthIn, int textureHeightIn)
	{
		super(modelSize, p_i1149_2_, textureWidthIn, textureHeightIn);
		
		this.walkSpeed = 1.1F / this.modelScale;
		
        this.bipedHead = new PMM_ModelRenderer(this, 0, 0);
        this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, modelSize);
        this.bipedHead.setRotationPoint(0.0F, 0.0F + p_i1149_2_, 0.0F);
        this.bipedHeadwear = new PMM_ModelRenderer(this, 32, 0);
        this.bipedHeadwear.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, modelSize + 0.5F);
        this.bipedBody = new PMM_ModelRenderer(this, 16, 16);
        this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, modelSize);
        this.bipedBody.setRotationPoint(0.0F, 0.0F + p_i1149_2_, 0.0F);
		this.bipedBodywear = new PMM_ModelRenderer(this, 16, 32);
		this.bipedBodywear.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, modelSize + 0.25F);

		this.bipedLeftArm = new PMM_ModelRenderer(this, 32, 48);
		this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
		this.bipedLeftArm.setRotationPoint(5.0F, 2.5F + p_i1149_2_, 0.0F);
		this.bipedRightArm = new PMM_ModelRenderer(this, 40, 16);
		this.bipedRightArm.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
		this.bipedRightArm.setRotationPoint(-5.0F, 2.5F + p_i1149_2_, 0.0F);
		this.bipedLeftArmwear = new PMM_ModelRenderer(this, 48, 48);
		this.bipedLeftArmwear.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);
		this.bipedRightArmwear = new PMM_ModelRenderer(this, 40, 32);
		this.bipedRightArmwear.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);

		this.bipedLeftLeg = new PMM_ModelRenderer(this, 16, 48);
		this.bipedLeftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
		this.bipedLeftLeg.setRotationPoint(2.2F, 12.0F + p_i1149_2_, 0.0F);
		this.bipedRightLeg = new PMM_ModelRenderer(this, 0, 16);
		this.bipedRightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
		this.bipedRightLeg.setRotationPoint(-2.2F, 12.0F + p_i1149_2_, 0.0F);
		this.bipedRightLegwear = new PMM_ModelRenderer(this, 0, 32);
		this.bipedRightLegwear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
		this.bipedLeftLegwear = new PMM_ModelRenderer(this, 0, 48);
		this.bipedLeftLegwear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);


		this.bipedUpperBoob = new PMM_ModelRenderer(this, 16, 21);
		this.bipedUpperBoob.addBox(-4.0F, 0.0F, -4.0F, 8, 4, 4, modelSize - 0.01F);
		this.bipedUpperBoob.setRotationPoint(0.0F, 0.0F, -2.0F);
		this.bipedLowerBoob = new PMM_ModelRenderer(this, 16, 25);
		this.bipedLowerBoob.addBox(-4.0F, 0.0F, -4.0F, 8, 3, 4, modelSize - 0.02F);
		this.bipedLowerBoob.setRotationPoint(0.0F, 0.0F, -4.0F);
		this.bipedUpperBoobwear = new PMM_ModelRenderer(this, 16, 36);
		this.bipedUpperBoobwear.addBox(-4.0F, 0.152F, -4.125F, 8, 4, 4, modelSize + 0.24F);
		this.bipedLowerBoobwear = new PMM_ModelRenderer(this, 16, 40);
		this.bipedLowerBoobwear.addBox(-4.0F, 0.125F, -4.125F, 8, 3, 4, modelSize + 0.23F);

		this.bipedLeftEyelid = new PMM_ModelRenderer(this, 12, 16);
		this.bipedLeftEyelid.addBox(-3.0F, -4.0F, 2.1F, 2, 2, 2, modelSize);
		this.bipedLeftEyelid.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedLeftEyelid.setRotateAngle(0.0F, PMM_Math.PI, 0.0F);
		this.bipedRightEyelid = new PMM_ModelRenderer(this, 12, 16);
		this.bipedRightEyelid.addBox(-3.0F, -4.0F, -4.1F, 2, 2, 2, modelSize);
		this.bipedRightEyelid.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.bipedAhoge = new PMM_ModelRenderer(this, 24, 0);
		this.bipedAhoge.addBox(-3.5F, -3.0F, -1.0F, 7, 3, 1, modelSize);
		this.bipedAhoge.setRotationPoint(0.0F, -8.0F, 0.0F);
		
		this.bipedBody.addChild(this.bipedRightArm);
		this.bipedBody.addChild(this.bipedLeftArm);
		this.bipedBody.addChild(this.bipedRightLeg);
		this.bipedBody.addChild(this.bipedLeftLeg);
		this.bipedBody.addChild(this.bipedUpperBoob);
		this.bipedUpperBoob.addChild(this.bipedLowerBoob);
		
		this.bipedHead.addChild(this.bipedHeadwear);
		this.bipedHead.addChild(this.bipedAhoge);
		this.bipedHead.addChild(this.bipedLeftEyelid);
		this.bipedHead.addChild(this.bipedRightEyelid);
		
		this.bipedBody.addChild(this.bipedBodywear);
		this.bipedLeftArm.addChild (this.bipedLeftArmwear);
		this.bipedRightArm.addChild(this.bipedRightArmwear);
		this.bipedLeftLeg.addChild (this.bipedLeftLegwear);
		this.bipedRightLeg.addChild(this.bipedRightLegwear);
		this.bipedUpperBoob.addChild(this.bipedUpperBoobwear);
		this.bipedLowerBoob.addChild(this.bipedLowerBoobwear);
	}

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	@Override
	public void render(Entity entityInIn, float limbSwingIn, float limbSwingAmountIn, float ageInTicksIn, float netHeadYawIn, float headPitchIn, float scaleFactorIn)
	{
		this.limbSwing = limbSwingIn;
		this.limbSwingAmount = limbSwingAmountIn;
		this.ageInTicks = ageInTicksIn;
		this.netHeadYaw = netHeadYawIn;
		this.headPitch = headPitchIn;
		this.scaleFactor = scaleFactorIn;
		this.entityIn = entityInIn;
		
		limbSwing *= this.walkSpeed + (this.isChild? 0.2F: 0.0F);
		if (this.doWalkBounding)
		{
			GlStateManager.translate(0.0F, MathHelper.abs(MathHelper.cos(this.limbSwing * 1.3314F)) * this.limbSwingAmount * 0.0625F * 4 * this.modelScale - 0.0625F * 2 * this.limbSwingAmount * (this.isChild? 0.5F: 1F) * this.modelScale, 0.0F);
		}
//		scale *= this.modelScale;
		this.setRotationAngles();
		GlStateManager.pushMatrix();

		if (this.isChild)
		{
			this.bipedHeadwear.isHidden = true;
			this.bipedLowerBoob.isHidden = true;
			this.bipedUpperBoob.isHidden = true;
			this.bipedLowerBoobwear.isHidden = true;
			this.bipedUpperBoobwear.isHidden = true;
			
			GlStateManager.translate(0.0F, 1.5F * (1F-this.modelScale), 0.0F);
			GlStateManager.scale(0.75F * this.modelScale, 0.75F * this.modelScale, 0.75F * this.modelScale);
			GlStateManager.translate(0.0F, 16.0F * this.scaleFactor * this.modelScale, 0.0F);
			this.bipedHead.render(this.scaleFactor, this.entityIn.hashCode());
			GlStateManager.popMatrix();
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.0F, 1.5F * (1F-this.modelScale), 0.0F);
			GlStateManager.scale(0.5F * this.modelScale, 0.5F * this.modelScale, 0.5F * this.modelScale);
			GlStateManager.translate(0.0F, 24.0F * this.scaleFactor * this.modelScale, 0.0F);
			this.bipedBody.render(this.scaleFactor, this.entityIn.hashCode());
		}
		else
		{
			GlStateManager.translate(0.0F, -1.5F * this.modelScale + 1.5F, 0.0F);
			GlStateManager.scale(this.modelScale, this.modelScale, this.modelScale);

			this.bipedHeadwear.isHidden = false;
			if (boobHeight > 0.000012)
			{
				if (boobHeight < 0.999996F)
				{
					
					this.bipedLowerBoob.isHidden = false;
					this.bipedUpperBoob.isHidden = false;
					this.bipedLowerBoobwear.isHidden = false;
					this.bipedUpperBoobwear.isHidden = false;
				}
				else {
					this.bipedLowerBoob.isHidden = true;
					this.bipedUpperBoob.isHidden = false;
					this.bipedLowerBoobwear.isHidden = true;
					this.bipedUpperBoobwear.isHidden = false;
				}
			}
			else {
				this.bipedLowerBoob.isHidden = true;
				this.bipedUpperBoob.isHidden = true;
				this.bipedLowerBoobwear.isHidden = true;
				this.bipedUpperBoobwear.isHidden = true;
			}
			
			this.bipedHead.render(this.scaleFactor);
			this.bipedBody.render(this.scaleFactor);
		}
		GlStateManager.popMatrix();
	}

	public void setRotationAngles()
	{
		for (ModelRenderer renderer : boxList) {
			if (renderer.getClass() == PMM_ModelRenderer.class)
			((PMM_ModelRenderer)renderer).reset();
		}
		
		// 頭の回転
		this.setHeadYawAndPitch();
		
		// 何もしてないときのモーション
		this.setStayAnimations();

		// 歩いてるときのモーション
        if (this.limbSwingAmount > 0.0F)
        {
        	this.setWalkingAnimations();
        }

		// 乗ってる時のモーション
		if (this.isRiding)
		{
			this.setRidingAnimations();
		}

		// 手に何か持ってるとき
		this.setHasAnythingAnimations(this.leftArmPose, this.rightArmPose);

		// 叩く時のモーション
		if (this.swingProgress > 0.0F)
		{
			this.setSwingProgressAnimations();
		}
		
		// スニーク時のモーション
        if (this.isSneak)
        {
        	this.setSneakAnimations();
        }

		// 弓持ってる時のモーション
		if (this.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW || this.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW)
		{
			this.setHasBowAnimations(this.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW, this.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW);
		}

		// ダメージ時のモーション
		if (this.entityHurtTime > 1)
		{
			this.setDamagedAnimations();
		}
		
		// 空中にいるの時のモーション
		else if (MathHelper.abs((float)this.entityMotionY) > 0.001F && !this.entityOnGround)
		{
			// 水中にいるとき
			if (this.entityIsInWater)
			{
				this.setSwimmingAnimations();
			} else 
			{
				this.setJumpAnimations();
			}
		}

		this.setAddAnimations();

		this.setBoobsAnimations();
		this.setAhogeAnimations();
		this.setTwinkleAnimations();
		
		float gapLY = 0.0F, gapRY = 0.0F;
		if (this.bipedLeftLeg.rotateAngleX < 0.0F)
		{
			this.bipedLeftLeg.rotationPointY += gapLY = (float)Math.sin(this.bipedLeftLeg.rotateAngleX) * 2.0F;
			this.bipedLeftLeg.rotationPointZ += (1F - (float)Math.cos(this.bipedLeftLeg.rotateAngleX)) * 2.0F;
		}
		else
		{
			// 0.523599rad '=, 30deg
			this.bipedLeftLeg.rotationPointY += gapLY = (float)Math.sin(-Math.min(this.bipedLeftLeg.rotateAngleX, 0.523599D)) * 2.0F;
			this.bipedLeftLeg.rotationPointZ -= (1F - (float)Math.cos(-Math.min(this.bipedLeftLeg.rotateAngleX, 0.523599D))) * 2.0F;
		}
		if (this.bipedRightLeg.rotateAngleX < 0.0F)
		{
			this.bipedRightLeg.rotationPointY += gapRY = (float)Math.sin(this.bipedRightLeg.rotateAngleX) * 2.0F;
			this.bipedRightLeg.rotationPointZ += (1F - (float)Math.cos(this.bipedRightLeg.rotateAngleX)) * 2.0F;
		}
		else
		{
			this.bipedRightLeg.rotationPointY += gapRY = (float)Math.sin(-Math.min(this.bipedRightLeg.rotateAngleX, 0.523599D)) * 2.0F;
			this.bipedRightLeg.rotationPointZ -= (1F - (float)Math.cos(-Math.min(this.bipedRightLeg.rotateAngleX, 0.523599D))) * 2.0F;
		}
		float y = Math.min(gapLY, gapRY);
		this.bipedBody.rotationPointY -= y;
		this.bipedHead.rotationPointY -= y;
	}


	
	/** 頭の回転 */
	protected void setHeadYawAndPitch()
	{
		this.bipedHead.rotateAngleY = this.netHeadYaw * PMM_Math.Deg2Rad;
		this.bipedHead.rotateAngleX = this.headPitch * PMM_Math.Deg2Rad;
	}

	/** あほげのアニメーション */
	protected void setAhogeAnimations()
	{
		if (ahogeSwing)
			this.bipedAhoge.rotateAngleX = MathHelper.cos(this.limbSwing * 0.6662F * 2F + (float)Math.PI * this.limbSwingAmount) * 0.2F * this.limbSwingAmount;
	}

	/** おっぱい部分のアニメーション */
	protected void setBoobsAnimations()
	{
		if (this.bipedUpperBoob.initRotateAngleX == 0.0F)
		{
			this.bipedUpperBoob.initRotationPointY = this.boobHeight;
			this.bipedUpperBoob.initRotateAngleX = -(float)Math.asin((double)this.boobHeight * 0.707106781F) + 1.570796326F;
			float f = this.boobHeight;
			if (this.boobHeight > 0.5F) f = 1.0F - f;
			this.bipedLowerBoob.initRotateAngleX = -(this.bipedUpperBoob.rotateAngleX - PMM_Math.PI_Half) * (2 + f);
		}
		
		this.bipedUpperBoob.rotationPointY += (this.boobHeight - MathHelper.abs(MathHelper.cos(this.limbSwing * 1.3314F + 0.5F)) * this.boobHeight*2) * this.limbSwingAmount;

		this.bipedUpperBoob.rotateAngleX += (MathHelper.abs(MathHelper.cos(this.limbSwing * 1.3314F + 0.75F)) * 30 * this.boobHeight * PMM_Math.Deg2Rad) * this.limbSwingAmount;
		float f = this.boobHeight;
		if (this.boobHeight > 0.5F) f = 1.0F - f;
		this.bipedLowerBoob.rotateAngleX = -(this.bipedUpperBoob.rotateAngleX - PMM_Math.PI_Half) * (2 + f);
		
	}

	/** まばたき */
	protected void setTwinkleAnimations()
	{
		Minecraft.getMinecraft(); // <- ?
        if (!this.twinklesTimes.containsKey(entityId))
        {
            if (this.twinklesTimes.size() > 100) { this.twinklesTimes.clear(); }
            this.twinklesTimes.put(entityId, (float)Math.random()*8F);
        }
        float twTime = this.twTimes.get(entityId);
		this.twTime -= 0.05F;
		float hash = (entityId/10) %1;
		if (this.twinkledNow)
		{
			if (this.twTime < 0.0F)
			{
				this.twinkledNow = false;
				this.twTime = (float)Math.random() * 8 + hash;
			}
		}
		else
		{
			if (this.twTime < 0.0F)
			{
				this.twinkledNow = true;
				this.twTime = (float)Math.random() + hash;
			}
		}
        
        this.twinklesTimes.put(entityId, twTime);
		this.bipedLeftEyelid.isHidden  = !this.twinkledNow;
		this.bipedRightEyelid.isHidden = !this.twinkledNow;
	}
	
	/** 何もしてないときのモーション */
	protected void setStayAnimations()
	{
		this.bipedRightArm.rotateAngleZ = MathHelper.cos(this.ageInTicks * 0.09F) * 0.05F + 0.05F;
        this.bipedLeftArm.rotateAngleZ  = -MathHelper.cos(this.ageInTicks * 0.09F) * 0.05F - 0.05F;
        this.bipedRightArm.rotateAngleX = MathHelper.sin(this.ageInTicks * 0.067F) * 0.05F;
        this.bipedLeftArm.rotateAngleX  = -MathHelper.sin(this.ageInTicks * 0.067F) * 0.05F;
	}
	
	/** 水中にいるときのモーション */
	protected void setSwimmingAnimations()
	{
		this.bipedRightLeg.rotateAngleY =  0.15F;
		this.bipedLeftLeg.rotateAngleY  = -0.15F;
		this.bipedRightLeg.rotateAngleX = MathHelper.cos(this.ageInTicks * 0.6F) * 1.4F;
		this.bipedLeftLeg.rotateAngleX  = MathHelper.cos(this.ageInTicks * 0.6F + (float)Math.PI) * 1.4F;
		this.bipedRightLeg.rotateAngleZ =  0.3F;
		this.bipedLeftLeg.rotateAngleZ  = -0.3F;
	}

	/** onGroundじゃない時のモーション */
	protected void setJumpAnimations()
	{
		float fallingSpeed = MathHelper.clamp((float)entityMotionY, -1.0F, 1.0F);
		this.bipedLeftArm.rotateAngleZ  = -90 * PMM_Math.Deg2Rad + fallingSpeed;
		this.bipedRightArm.rotateAngleZ =  90 * PMM_Math.Deg2Rad - fallingSpeed;
		this.bipedLeftLeg.rotateAngleY = -fallingSpeed / 3F;
		this.bipedLeftLeg.rotateAngleY =  fallingSpeed / 3F;
		this.bipedLeftLeg.rotateAngleX  = fallingSpeed;
		this.bipedRightLeg.rotateAngleX = fallingSpeed;
	}

	/** ダメージ受けた時のアニメーション  */
	protected void setDamagedAnimations()
	{
		this.setWalkingAnimations();
		this.bipedHead.rotateAngleX = 15F * PMM_Math.Deg2Rad;
		this.bipedLeftLeg.rotateAngleX  -= 20F * PMM_Math.Deg2Rad;
		this.bipedRightLeg.rotateAngleX -= 20F * PMM_Math.Deg2Rad;
		this.bipedLeftArm.rotateAngleZ  -= 80F * PMM_Math.Deg2Rad;
		this.bipedRightArm.rotateAngleZ += 80F * PMM_Math.Deg2Rad;
		this.bipedLeftArm.rotateAngleY -= 80F * PMM_Math.Deg2Rad;
		this.bipedRightArm.rotateAngleY += 80F * PMM_Math.Deg2Rad;
	}

	/** 弓矢持ってる時のアニメーション */
	protected void setHasBowAnimations(boolean hasBowRight, boolean hasBowLeft)
	{
		this.bipedRightArm.rotateAngleY = -0.1F + this.bipedHead.rotateAngleY - (hasBowLeft? 0.4F: 0.0F);
		this.bipedLeftArm.rotateAngleY = 0.1F + this.bipedHead.rotateAngleY + (hasBowRight? 0.4F: 0.0F);
		this.bipedRightArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
		this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
	}
	
	/** 歩いてる時のモーション */
	protected void setWalkingAnimations()
	{
		this.bipedRightArm.rotateAngleY = 0.5F * this.limbSwingAmount;
		this.bipedLeftArm.rotateAngleY  = -0.5F * this.limbSwingAmount;
		this.bipedRightArm.rotateAngleX = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI) * 2.0F * this.limbSwingAmount * 0.5F;
		this.bipedLeftArm.rotateAngleX  = MathHelper.cos(this.limbSwing * 1.3324F) * 2.0F * this.limbSwingAmount * 0.5F;
		this.bipedRightArm.rotateAngleZ = 0.5F * this.limbSwingAmount;
		this.bipedLeftArm.rotateAngleZ  = -0.5F * this.limbSwingAmount;
		this.bipedRightLeg.rotateAngleZ =  0.075F * this.limbSwingAmount;
		this.bipedLeftLeg.rotateAngleZ  = -0.075F * this.limbSwingAmount;
		this.bipedRightLeg.rotateAngleY =  0.15F * this.limbSwingAmount;
		this.bipedLeftLeg.rotateAngleY  = -0.15F * this.limbSwingAmount;
		this.bipedRightLeg.rotateAngleX = MathHelper.cos(this.limbSwing * 1.3324F) * 1.4F * this.limbSwingAmount;
		this.bipedLeftLeg.rotateAngleX  = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI) * 1.4F * this.limbSwingAmount;
	}
	
	/** 何かに乗ってる時のモーション */
	protected void setRidingAnimations()
	{
		this.bipedRightArm.rotateAngleX = -((float)Math.PI / 5F);
		this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 5F);
		this.bipedRightLeg.rotateAngleX = -1.4137167F;
		this.bipedRightLeg.rotateAngleY = ((float)Math.PI / 10F);
		this.bipedRightLeg.rotateAngleZ = 0.07853982F;
		this.bipedLeftLeg.rotateAngleX = -1.4137167F;
		this.bipedLeftLeg.rotateAngleY = -((float)Math.PI / 10F);
		this.bipedLeftLeg.rotateAngleZ = -0.07853982F;
	}
	
	/** 手に何か持ってる時のアニメーション */
	protected void setHasAnythingAnimations(ArmPose leftArmPose, ArmPose rightArmPose)
	{
		switch (leftArmPose)
		{
		case BLOCK:
			this.bipedLeftArm.rotateAngleX = this.bipedLeftArm.rotateAngleX * 0.5F - 0.9424779F;
			this.bipedLeftArm.rotateAngleY = 0.5235988F;
			break;
		case ITEM:
			this.bipedLeftArm.rotateAngleX = this.bipedLeftArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F);
			this.bipedLeftArm.rotateAngleY = 0.0F;
			break;
		default:
			break;
		}

		switch (rightArmPose)
		{
		case BLOCK:
			this.bipedRightArm.rotateAngleX = this.bipedRightArm.rotateAngleX * 0.5F - 0.9424779F;
			this.bipedRightArm.rotateAngleY = -0.5235988F;
			break;
		case ITEM:
			this.bipedRightArm.rotateAngleX = this.bipedRightArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F);
			this.bipedRightArm.rotateAngleY = 0.0F;
			break;
		default:
			break;
		}
	}

	/** 叩くときのモーション*/
	protected void setSwingProgressAnimations()
	{
		EnumHandSide enumhandside = this.getMainHand(this.entityIn);
		PMM_ModelRenderer modelrenderer = (PMM_ModelRenderer) this.getArmForSide(enumhandside);
		float f1 = this.swingProgress;
		this.bipedBody.rotateAngleY = MathHelper.sin(MathHelper.sqrt(f1) * ((float)Math.PI * 2F)) * 0.2F;

		if (enumhandside == EnumHandSide.LEFT)
		{
			this.bipedBody.rotateAngleY *= -1.0F;
		}

		this.bipedRightArm.rotationPointZ = MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedRightArm.rotationPointX = -MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedLeftArm.rotationPointZ = -MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedLeftArm.rotationPointX = MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
		this.bipedRightArm.rotateAngleY = this.bipedBody.rotateAngleY;
		this.bipedLeftArm.rotateAngleY = this.bipedBody.rotateAngleY;
		this.bipedLeftArm.rotateAngleX += this.bipedBody.rotateAngleY;
		f1 = 1.0F - this.swingProgress;
		f1 = f1 * f1;
		f1 = f1 * f1;
		f1 = 1.0F - f1;
		float f2 = MathHelper.sin(f1 * (float)Math.PI);
		float f3 = MathHelper.sin(this.swingProgress * (float)Math.PI) * -(this.bipedHead.rotateAngleX - 0.7F) * 0.75F;
		modelrenderer.rotateAngleX = (float)(modelrenderer.rotateAngleX - (f2 * 1.2D + f3));
		modelrenderer.rotateAngleY += this.bipedBody.rotateAngleY * 2.0F;
		modelrenderer.rotateAngleZ += MathHelper.sin(this.swingProgress * (float)Math.PI) * -0.4F;
	}
	
	/** スニーク時のモーション */
	protected void setSneakAnimations()
	{
        this.bipedBody.rotateAngleX += 0.5F;
        this.bipedRightArm.rotateAngleX += 0.2F;
        this.bipedLeftArm.rotateAngleX += 0.2F;
        this.bipedRightArm.rotateAngleZ += 0.1F;
        this.bipedLeftArm.rotateAngleZ -= 0.1F;
        this.bipedRightLeg.rotateAngleX -= 0.75F;
        this.bipedLeftLeg.rotateAngleX -= 0.75F;
        this.bipedHead.rotationPointY += 3.0F;
        this.bipedBody.rotationPointY += 3.0F;
        this.bipedHead.rotationPointZ -= 1.5F;
        this.bipedBody.rotationPointZ -= 1.5F;
	}

	protected void setAddAnimations()
	{
	}




	@Override
	public void setModelAttributes(ModelBase model)
	{
		super.setModelAttributes(model);

		if (model instanceof PMM_ModelBiped)
		{
			PMM_ModelBiped modelbiped = (PMM_ModelBiped)model;
			this.isSneak = modelbiped.isSneak;
		}
	}
}
