package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSquid extends PMM_ModelBiped
{
	/** The squid's body */
	// ModelRenderer squidBody;
	/** The squid's tentacles */
	// ModelRenderer[] squidTentacles = new ModelRenderer[8];

	public PMM_ModelSquid() {
		super(0.0F, 0.0F, 64, 64);
		this.doWalkBounding = false;
	}

	/**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
	@Override
    public void setRotationAngles()
    {
    	Minecraft.getMinecraft();
		float time = (float)Minecraft.getSystemTime() % 1000 / 1000F;
    	time *= 12F;
    	ageInTicks *= 1.2F;
        
    	this.bipedHead.rotateAngleX = -45F * PMM_Math.Deg2Rad;
    	
    	this.bipedLeftArm.rotateAngleY = -MathHelper.sin(this.ageInTicks * (float)Math.PI) * 90 * PMM_Math.Deg2Rad;
    	this.bipedLeftArm.rotateAngleX = -180F * PMM_Math.Deg2Rad * this.ageInTicks;
    	this.bipedRightArm.rotateAngleY = MathHelper.sin(this.ageInTicks * (float)Math.PI) * 90 * PMM_Math.Deg2Rad;
    	this.bipedRightArm.rotateAngleX = -180F * PMM_Math.Deg2Rad * this.ageInTicks;

    	this.bipedLeftLeg.rotateAngleX = -MathHelper.sin(time) * 45 * PMM_Math.Deg2Rad * (1F + this.ageInTicks)/ 2F;
    	this.bipedRightLeg.rotateAngleX = MathHelper.sin(time) * 45 * PMM_Math.Deg2Rad * (1F + this.ageInTicks)/ 2F;
    	

    	// �_���[�W�󂯂��Ƃ�
        if (this.entityHurtTime > 1)
        {
        	this.bipedHead.rotateAngleX = 30F * PMM_Math.Deg2Rad;
        	
        	this.bipedLeftLeg.rotateAngleX  = -45F * PMM_Math.Deg2Rad;
        	this.bipedLeftLeg.rotateAngleY  =  -10F * PMM_Math.Deg2Rad;
        	this.bipedRightLeg.rotateAngleX = -45F * PMM_Math.Deg2Rad;
        	this.bipedRightLeg.rotateAngleY =   10F * PMM_Math.Deg2Rad;
        }

        this.setAhogeAnimations();
        this.setBoobsAnimations();
        this.setTwinkleAnimations();
    }

	/**
	 * Sets the models various rotation angles then renders the model.
	 */
	/*
	 * public void render(Entity entityIn, float limbSwing, float
	 * limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch,
	 * float scale) { this.setRotationAngles(limbSwing, limbSwingAmount,
	 * ageInTicks, netHeadYaw, headPitch, scale, entityIn);
	 * this.squidBody.render(scale);
	 * 
	 * for (ModelRenderer modelrenderer : this.squidTentacles) {
	 * modelrenderer.render(scale); } }
	 */
}