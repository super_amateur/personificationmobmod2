package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerBipedSubParts;
import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.ModelBiped.ArmPose;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.MathHelper;

public class PMM_ModelBipedSubParts<T extends PMM_ModelBiped> extends ModelBase
{
	private T baseModel;
	
	public PMM_ModelRenderer bipedCopyBody;
	
	public PMM_ModelRenderer bipedPartLeftBackSkirtLong;
	public PMM_ModelRenderer bipedPartLeftFrontSkirtLong;
	public PMM_ModelRenderer bipedPartRightBackSkirtLong;
	public PMM_ModelRenderer bipedPartRightFrontSkirtLong;
	public PMM_ModelRenderer bipedPartsSkirtShort;

    public PMM_ModelBipedSubParts(PMM_LayerBipedSubParts<?,?> layer, float scale)
    {
    	this.baseModel = (T)layer.baseRenderer.getMainModel();
        this.textureWidth = 64;
        this.textureHeight = 64;
        
        this.bipedCopyBody = new PMM_ModelRenderer(this, false);
        this.bipedCopyBody.copyCompletly(this.baseModel.bipedBody);
        
        this.bipedPartLeftBackSkirtLong = new PMM_ModelRenderer(this, 0, 0);
        this.bipedPartLeftBackSkirtLong.addBox(-8.0F, 0.0F, -4.0F, 8, 10, 4, 0.25F + scale);
        this.bipedPartLeftBackSkirtLong.setRotationPoint(4.0F, 10.0F, 2.0F);
        this.bipedPartLeftFrontSkirtLong = new PMM_ModelRenderer(this, 0, 0);
        this.bipedPartLeftFrontSkirtLong.addBox(-8.0F, 0.0F, 0.0F, 8, 10, 4, 0.25F + scale);
        this.bipedPartLeftFrontSkirtLong.setRotationPoint(4.0F, 10.0F, -2.0F);
        this.bipedPartRightBackSkirtLong = new PMM_ModelRenderer(this, 0, 0);
        this.bipedPartRightBackSkirtLong.addBox(0.0F, 0.0F, -4.0F, 8, 10, 4, 0.25F + scale);
        this.bipedPartRightBackSkirtLong.setRotationPoint(-4.0F, 10.0F, 2.0F);
        this.bipedPartRightFrontSkirtLong = new PMM_ModelRenderer(this, 0, 0);
        this.bipedPartRightFrontSkirtLong.addBox(0.0F, 0.0F, 0.0F, 8, 10, 4, 0.25F + scale);
        this.bipedPartRightFrontSkirtLong.setRotationPoint(-4.0F, 10.0F, -2.0F);
        
        this.bipedPartsSkirtShort = new PMM_ModelRenderer(this, 0, 14);
        this.bipedPartsSkirtShort.addBox(-6.0F, 0.0F, -4.0F, 12, 8, 9, 0.0F + scale);
        this.bipedPartsSkirtShort.setRotationPoint(0.0F, 10.0F, 0.0F);
        
//        this.bipedCopyBody.addChild(this.bipedPartLeftBackSkirtLong);
//        this.bipedCopyBody.addChild(this.bipedPartLeftFrontSkirtLong);
//        this.bipedCopyBody.addChild(this.bipedPartRightBackSkirtLong);
//        this.bipedCopyBody.addChild(this.bipedPartRightFrontSkirtLong);
        
        this.bipedCopyBody.addChild(this.bipedPartsSkirtShort);
    }

	@Override
	public void render(Entity entityInIn, float limbSwingIn, float limbSwingAmountIn, float ageInTicksIn, float netHeadYawIn, float headPitchIn, float scaleFactorIn)
	{
		float modelScale = this.baseModel.modelScale;
		
		this.baseModel.limbSwing *= this.baseModel.walkSpeed + (this.baseModel.isChild? 0.2F: 0.0F);
		if (this.baseModel.doWalkBounding)
		{
			GlStateManager.translate(0.0F, MathHelper.abs(MathHelper.cos(this.baseModel.limbSwing * 1.3314F)) * this.baseModel.limbSwingAmount * 0.0625F * 4 * modelScale - 0.0625F * 2 * this.baseModel.limbSwingAmount * (this.isChild? 0.5F: 1F) * modelScale, 0.0F);
		}
//		scale *= this.modelScale;
		this.setRotationAngles();
		GlStateManager.pushMatrix();

		if (this.isChild)
		{
//			this.bipedHeadwear.isHidden = true;
//			this.bipedLowerBoob.isHidden = true;
//			this.bipedUpperBoob.isHidden = true;
//			this.bipedLowerBoobwear.isHidden = true;
//			this.bipedUpperBoobwear.isHidden = true;
			
			GlStateManager.translate(0.0F, 1.5F * (1F-modelScale), 0.0F);
			GlStateManager.scale(0.75F * modelScale, 0.75F * modelScale, 0.75F * modelScale);
			GlStateManager.translate(0.0F, 16.0F * this.baseModel.scaleFactor * modelScale, 0.0F);
//			this.bipedHead.render(this.scaleFactor);
			GlStateManager.popMatrix();
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.0F, 1.5F * (1F-modelScale), 0.0F);
			GlStateManager.scale(0.5F * modelScale, 0.5F * modelScale, 0.5F * modelScale);
			GlStateManager.translate(0.0F, 24.0F * this.baseModel.scaleFactor * modelScale, 0.0F);
			this.bipedCopyBody.render(this.baseModel.scaleFactor);
		}
		else
		{
			GlStateManager.translate(0.0F, -1.5F * modelScale + 1.5F, 0.0F);
			GlStateManager.scale(modelScale, modelScale, modelScale);

//			this.bipedHeadwear.isHidden = false;
//			if (boobHeight > 0.000012)
//			{
//				if (boobHeight < 0.999996F)
//				{
//					
//					this.bipedLowerBoob.isHidden = false;
//					this.bipedUpperBoob.isHidden = false;
//					this.bipedLowerBoobwear.isHidden = false;
//					this.bipedUpperBoobwear.isHidden = false;
//				}
//				else {
//					this.bipedLowerBoob.isHidden = true;
//					this.bipedUpperBoob.isHidden = false;
//					this.bipedLowerBoobwear.isHidden = true;
//					this.bipedUpperBoobwear.isHidden = false;
//				}
//			}
//			else {
//				this.bipedLowerBoob.isHidden = true;
//				this.bipedUpperBoob.isHidden = true;
//				this.bipedLowerBoobwear.isHidden = true;
//				this.bipedUpperBoobwear.isHidden = true;
//			}
//			
//			this.bipedHead.render(this.scaleFactor);
			this.bipedCopyBody.render(this.baseModel.scaleFactor);
		}
		GlStateManager.popMatrix();
	}

	public void setRotationAngles()
	{
		this.bipedCopyBody.copyCompletly(this.baseModel.bipedBody);
		
		// 何もしてないときのモーション
		this.setStayAnimations();

		// 歩いてるときのモーション
        if (this.baseModel.limbSwingAmount > 0.0F)
        {
        	this.setWalkingAnimations();
        }

		// 乗ってる時のモーション
		if (this.baseModel.isRiding)
		{
			this.setRidingAnimations();
		}

		// 手に何か持ってるとき
		this.setHasAnythingAnimations(this.baseModel.leftArmPose, this.baseModel.rightArmPose);

		// 叩く時のモーション
		if (this.baseModel.swingProgress > 0.0F)
		{
			this.setSwingProgressAnimations();
		}
		
		// スニーク時のモーション
        if (this.baseModel.isSneak)
        {
        	this.setSneakAnimations();
        }

		// 弓持ってる時のモーション
		if (this.baseModel.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW || this.baseModel.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW)
		{
			this.setHasBowAnimations(this.baseModel.rightArmPose == ModelBiped.ArmPose.BOW_AND_ARROW, this.baseModel.leftArmPose == ModelBiped.ArmPose.BOW_AND_ARROW);
		}

		// ダメージ時のモーション
		if (this.baseModel.entityHurtTime > 1)
		{
			this.setDamagedAnimations();
		}
		
		// 空中にいるの時のモーション
		else if (MathHelper.abs((float)this.baseModel.entityMotionY) > 0.001F && !this.baseModel.entityOnGround)
		{
			// 水中にいるとき
			if (this.baseModel.entityIsInWater)
			{
				this.setSwimmingAnimations();
			} else 
			{
				this.setJumpAnimations();
			}
		}

		this.setAddAnimations();
	}


	
	
	/** 何もしてないときのモーション */
	protected void setStayAnimations()
	{
	}
	
	/** 水中にいるときのモーション */
	protected void setSwimmingAnimations()
	{
	}

	/** onGroundじゃない時のモーション */
	protected void setJumpAnimations()
	{
	}

	/** ダメージ受けた時のアニメーション  */
	protected void setDamagedAnimations()
	{
	}

	/** 弓矢持ってる時のアニメーション */
	protected void setHasBowAnimations(boolean hasBowRight, boolean hasBowLeft)
	{
	}
	
	/** 歩いてる時のモーション */
	protected void setWalkingAnimations()
	{
	}
	
	/** 何かに乗ってる時のモーション */
	protected void setRidingAnimations()
	{
	}
	
	/** 手に何か持ってる時のアニメーション */
	protected void setHasAnythingAnimations(ArmPose leftArmPose, ArmPose rightArmPose)
	{
	}

	/** 叩くときのモーション*/
	protected void setSwingProgressAnimations()
	{
	}
	
	/** スニーク時のモーション */
	protected void setSneakAnimations()
	{
	}

	protected void setAddAnimations()
	{
		this.bipedPartLeftBackSkirtLong.rotateAngleZ = Math.min(-0.1F, this.baseModel.bipedLeftLeg.rotateAngleZ - 0.05F);
		this.bipedPartLeftBackSkirtLong.rotateAngleX = Math.max(0.1F, this.baseModel.bipedLeftLeg.rotateAngleX + 0.05F) + 30F * PMM_Math.Deg2Rad * this.baseModel.limbSwingAmount;
		
		this.bipedPartLeftFrontSkirtLong.rotateAngleZ = Math.min(-0.1F, this.baseModel.bipedLeftLeg.rotateAngleZ - 0.05F);
		this.bipedPartLeftFrontSkirtLong.rotateAngleX = Math.min(-0.1F, this.baseModel.bipedLeftLeg.rotateAngleX - 0.05F);
		
		this.bipedPartRightBackSkirtLong.rotateAngleZ = Math.max(0.1F, this.baseModel.bipedRightLeg.rotateAngleZ + 0.05F);
		this.bipedPartRightBackSkirtLong.rotateAngleX = Math.max(0.1F, this.baseModel.bipedRightLeg.rotateAngleX + 0.05F) + 30F * PMM_Math.Deg2Rad * this.baseModel.limbSwingAmount;
		this.bipedPartRightFrontSkirtLong.rotateAngleZ = Math.max(0.1F, this.baseModel.bipedRightLeg.rotateAngleZ + 0.05F);
		this.bipedPartRightFrontSkirtLong.rotateAngleX = Math.min(-0.1F, this.baseModel.bipedRightLeg.rotateAngleX - 0.05F);
	}

    public void setModelAttributes(T model)
    {
    	this.baseModel = model;
        this.swingProgress = model.swingProgress;
        this.isRiding = model.isRiding;
        this.isChild = model.isChild;
    }
}
