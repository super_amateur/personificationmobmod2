package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.AbstractSkeleton;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSkeleton extends PMM_ModelBiped
{
	public ItemStack entityHeldItemInMainHand;
	public EnumHandSide entityPrimaryHand;
	
    public PMM_ModelSkeleton()
    {
        this(0.0F, false);
    }

    public PMM_ModelSkeleton(float modelSize, boolean p_i46303_2_)
    {
        super(modelSize, 0.0F, 64, 64);
        this.boobHeight = 0.7F;
    }

    @Override
	public void setAddAnimations()
    {
        this.rightArmPose = PMM_ModelBiped.ArmPose.EMPTY;
        this.leftArmPose = PMM_ModelBiped.ArmPose.EMPTY;
        ItemStack itemstack = this.entityHeldItemInMainHand;

        if (itemstack.getItem() == Items.BOW && ((AbstractSkeleton)entityIn).isSwingingArms())
        {
            if (this.entityPrimaryHand == EnumHandSide.RIGHT)
            {
                this.rightArmPose = PMM_ModelBiped.ArmPose.BOW_AND_ARROW;
            }
            else
            {
                this.leftArmPose = PMM_ModelBiped.ArmPose.BOW_AND_ARROW;
            }
        }

        if (this.limbSwingAmount > 0 && (itemstack.isEmpty() || itemstack.getItem() != Items.BOW))
        {
            float f = MathHelper.sin(this.swingProgress * (float)Math.PI);
            float f1 = MathHelper.sin((1.0F - (1.0F - this.swingProgress) * (1.0F - this.swingProgress)) * (float)Math.PI);
            this.bipedRightArm.rotateAngleZ = 0.0F;
            this.bipedLeftArm.rotateAngleZ = 0.0F;
            this.bipedRightArm.rotateAngleY = -(0.1F - f * 0.6F);
            this.bipedLeftArm.rotateAngleY = 0.1F - f * 0.6F;
            this.bipedRightArm.rotateAngleX = -((float)Math.PI / 2F);
            this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 2F);
            this.bipedRightArm.rotateAngleX -= f * 1.2F - f1 * 0.4F;
            this.bipedLeftArm.rotateAngleX -= f * 1.2F - f1 * 0.4F;
            this.bipedRightArm.rotateAngleZ += MathHelper.cos(this.ageInTicks * 0.09F) * 0.05F + 0.05F;
            this.bipedLeftArm.rotateAngleZ -= MathHelper.cos(this.ageInTicks * 0.09F) * 0.05F + 0.05F;
            this.bipedRightArm.rotateAngleX += MathHelper.sin(this.ageInTicks * 0.067F) * 0.05F;
            this.bipedLeftArm.rotateAngleX -= MathHelper.sin(this.ageInTicks * 0.067F) * 0.05F;
        }
    }

	public void postRenderArm(float scale, EnumHandSide side)
    {
        float f = side == EnumHandSide.RIGHT ? 1.0F : -1.0F;
        ModelRenderer modelrenderer = this.getArmForSide(side);
        modelrenderer.rotationPointX += f;
        modelrenderer.postRender(scale);
        modelrenderer.rotationPointX -= f;
    }
}