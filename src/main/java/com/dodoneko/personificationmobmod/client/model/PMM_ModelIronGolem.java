package com.dodoneko.personificationmobmod.client.model;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelIronGolem extends PMM_ModelBiped
{
    public PMM_ModelIronGolem()
    {
        this(0.0F);
    }

    public PMM_ModelIronGolem(float p_i1161_1_)
    {
        this(p_i1161_1_, -7.0F);
    }

    public PMM_ModelIronGolem(float p_i46362_1_, float p_i46362_2_)
    {
    	super();
    	this.modelScale = 1.8f;
    	this.boobHeight = 0.3f;
    }
}