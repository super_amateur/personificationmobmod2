package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.passive.EntitySheep;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSheep extends PMM_ModelBiped
{
    public PMM_ModelSheep()
    {
    	this(0.0F);
    }
    
    public PMM_ModelSheep(float modelSize)
    {
        super(modelSize);
        this.modelScale = 0.5f;
        this.boobHeight = 0.6f;
    }
}