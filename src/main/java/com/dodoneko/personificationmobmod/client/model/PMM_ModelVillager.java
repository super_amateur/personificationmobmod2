package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.EntityCreature;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelVillager extends PMM_ModelBiped
{

	public PMM_ModelVillager(float scale)
    {
        this(scale, 0.0F, 64, 64);
    }

    public PMM_ModelVillager(float scale, float p_i1164_2_, int width, int height)
    {
    	super();
    }
}