package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelBat extends PMM_ModelBiped
{
	
	public boolean entityIsBatHanging;

    public PMM_ModelBat()
    {
    	super();
    	this.boobHeight = 0.15F;
    	this.modelScale = 1.75F;
    }
    
	@Override
	public void render(Entity entityInIn, float limbSwingIn, float limbSwingAmountIn, float ageInTicksIn, float netHeadYawIn, float headPitchIn, float scaleFactorIn)
	{
		this.limbSwing = limbSwingIn;
		this.limbSwingAmount = limbSwingAmountIn;
		this.ageInTicks = ageInTicksIn;
		this.netHeadYaw = netHeadYawIn;
		this.headPitch = headPitchIn;
		this.scaleFactor = scaleFactorIn;
		this.entityIn = entityInIn;
		
		limbSwing *= this.walkSpeed + (this.isChild? 0.5F: 0.0F);
		if (this.doWalkBounding)
		{
//			GlStateManager.translate(0.0F, MathHelper.abs(MathHelper.cos(this.limbSwing * 1.3314F)) * this.limbSwingAmount * 0.0625F * 4 * this.modelScale - 0.0625F * 2 * this.limbSwingAmount * (this.isChild? 0.5F: 1F) * this.modelScale, 0.0F);
		}
		scaleFactor *= this.modelScale;
		this.setRotationAngles();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(0.0F, -1.5F * this.modelScale + 1.5F, 0.0F);
		GlStateManager.scale(0.75F, 0.75F, 0.75F);
		GlStateManager.translate(0.0F, 16.0F * scaleFactor, 0.0F);
		this.bipedHead.render(this.scaleFactor, this.entityIn.hashCode());
		GlStateManager.popMatrix();

		GlStateManager.pushMatrix();
		GlStateManager.translate(0.0F, -1.5F * this.modelScale + 1.5F, 0.0F);
		GlStateManager.scale(0.5F, 0.5F, 0.5F);
		GlStateManager.translate(0.0F, 24.0F * scaleFactor, 0.0F);
		this.bipedHeadwear.isHidden = false;
		if (boobHeight > 0.000012)
		{
			if (boobHeight < 0.999996F)
			{

				this.bipedLowerBoob.isHidden = false;
				this.bipedUpperBoob.isHidden = false;
				this.bipedLowerBoobwear.isHidden = false;
				this.bipedUpperBoobwear.isHidden = false;
			}
			else {
				this.bipedLowerBoob.isHidden = true;
				this.bipedUpperBoob.isHidden = false;
				this.bipedLowerBoobwear.isHidden = true;
				this.bipedUpperBoobwear.isHidden = false;
			}
		}
		else {
			this.bipedLowerBoob.isHidden = true;
			this.bipedUpperBoob.isHidden = true;
			this.bipedLowerBoobwear.isHidden = true;
			this.bipedUpperBoobwear.isHidden = true;
		}
		this.bipedBody.render(this.scaleFactor, this.entityIn.hashCode());
		GlStateManager.popMatrix();
	}

	@Override
    public void setAddAnimations()
    {
		// ぶら下がってる時
        if (this.entityIsBatHanging)
        {
        	this.setSneakAnimations();
        	
            this.bipedHead.rotateAngleY *= -1;
            this.bipedHead.rotateAngleX += (float)Math.PI;
            this.bipedBody.rotateAngleX += (float)Math.PI;

        	this.bipedBody.rotationPointY *= -1;
        	this.bipedHead.rotationPointY *= -1;
        	this.bipedBody.rotationPointY -= 3;
        	this.bipedHead.rotationPointY -= 3;
        }
        else
        // ぶら下がってない時
        {
            this.bipedHead.rotateAngleX = this.headPitch * 0.017453292F;
            this.bipedHead.rotateAngleY = this.netHeadYaw * 0.017453292F;
            this.bipedBody.rotateAngleX = ((float)Math.PI / 4F) + MathHelper.cos(this.ageInTicks * 0.1F) * 0.15F;
            
            // 羽ばたく
            float tcos = PMM_Math.normalizedSinWave(ageInTicks, 1.3F);
            this.bipedRightArm.rotateAngleX = PMM_Math.lerp(-75F, -75F, tcos) * PMM_Math.Deg2Rad;
            this.bipedRightArm.rotateAngleY = PMM_Math.lerp(-30F, 150F, tcos) * PMM_Math.Deg2Rad;
            this.bipedRightArm.rotateAngleZ = PMM_Math.lerp(0F, 45F, tcos) * PMM_Math.Deg2Rad;
            this.bipedLeftArm.rotateAngleX =  this.bipedRightArm.rotateAngleX;
            this.bipedLeftArm.rotateAngleY = -this.bipedRightArm.rotateAngleY;
            this.bipedLeftArm.rotateAngleZ = -this.bipedRightArm.rotateAngleZ;
        }
    }
}