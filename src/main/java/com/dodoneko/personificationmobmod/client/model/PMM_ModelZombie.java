package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.monster.EntityZombie;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelZombie extends PMM_ModelBiped
{
    public PMM_ModelZombie()
    {
        this(0.0F, false);
    }

    public PMM_ModelZombie(float modelSize, boolean p_i1168_2_)
    {
        super(modelSize);
        boobHeight = 0.2F;
        modelScale = 0.9F;
    }
}