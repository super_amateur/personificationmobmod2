package com.dodoneko.personificationmobmod.client.model;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelEnderman extends PMM_ModelBiped
{
    /** Is the enderman carrying a block? */
    public boolean isCarrying;
    /** Is the enderman attacking an entity? */
    public boolean isAttacking;
    
    public PMM_ModelRenderer endermanHeldBlock;

    public PMM_ModelEnderman(float modelSize)
    {
        super(modelSize, 0.0F, 64, 64);
        this.modelScale = 0.8F;
        this.boobHeight = 0.75F;
        this.doWalkBounding = false;
        
        this.endermanHeldBlock = new PMM_ModelRenderer(this);
        this.bipedBody.addChild(this.endermanHeldBlock);
    }

//	@Override
//	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
//	{
//		super.render(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
//	}

    @Override
    public void setAddAnimations()
    {
        if (this.isCarrying)
        {
            this.bipedRightArm.rotateAngleX = -0.5F;
            this.bipedLeftArm.rotateAngleX  = -0.5F;
            this.bipedRightArm.rotateAngleZ =  0.05F;
            this.bipedLeftArm.rotateAngleZ  = -0.05F;
            this.bipedRightArm.rotateAngleY = -0.01F;
            this.bipedLeftArm.rotateAngleY  =  0.01F;
        }

        if (this.isAttacking)
        {
        this.bipedLeftArm.rotateAngleX = -(MathHelper.sin(this.ageInTicks * 0.5F) * 20.0F + 60F) * PMM_Math.Deg2Rad;
		this.bipedLeftArm.rotateAngleZ =  (MathHelper.cos(this.ageInTicks * 0.09F) *  7.5F - 45F) * PMM_Math.Deg2Rad;
        }
    }
    
    @Override
	protected void setWalkingAnimations()
	{
	}
    
    @Override
	protected void setStayAnimations()
	{
    	float f = MathHelper.abs(MathHelper.cos(this.ageInTicks * 0.18F)) * this.modelScale * 0.0625F * 4 - 0.0625F * 2 * (this.isChild? 0.5F: 1F);
    	this.bipedHead.rotationPointY = f * 20F - 25F;
    	this.bipedBody.rotationPointY = f * 20F - 25F;
    	this.endermanHeldBlock.rotationPointY = f - 0.2F;
    	
    	this.bipedBody.rotateAngleX = -10F * PMM_Math.Deg2Rad;
    	
        this.bipedRightArm.rotateAngleX = -(MathHelper.sin(this.ageInTicks * 0.09F) * 15.0F + 30F) * PMM_Math.Deg2Rad;
        this.bipedLeftArm.rotateAngleX  = -(MathHelper.sin(this.ageInTicks * 0.09F) * 7.5F + 30F) * PMM_Math.Deg2Rad;
		this.bipedRightArm.rotateAngleZ = -(MathHelper.cos(this.ageInTicks * 0.09F) * 7.5F - 60F) * PMM_Math.Deg2Rad;
        this.bipedLeftArm.rotateAngleZ  =  (MathHelper.cos(this.ageInTicks * 0.09F) * 7.5F - 60F) * PMM_Math.Deg2Rad;

        this.bipedRightLeg.rotateAngleZ =  0.0F;
        this.bipedLeftLeg.rotateAngleZ  =  0.0F;
        this.bipedRightLeg.rotateAngleX =  (MathHelper.sin(this.ageInTicks * 0.09F) * 10.0F + 30F) * PMM_Math.Deg2Rad;
        this.bipedLeftLeg.rotateAngleX  =  (MathHelper.sin(this.ageInTicks *-0.09F) * 10.0F + 30F) * PMM_Math.Deg2Rad;
		this.bipedRightLeg.rotateAngleY =  (MathHelper.cos(this.ageInTicks * 0.09F) * 1.5F - 7.5F) * PMM_Math.Deg2Rad;
        this.bipedLeftLeg.rotateAngleY  = -(MathHelper.cos(this.ageInTicks * 0.09F) * 1.5F - 7.5F) * PMM_Math.Deg2Rad;
        
        this.endermanHeldBlock.rotationPointZ = -0.5F;
	}
}