package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class PMM_ModelSpider extends PMM_ModelBiped {

    public PMM_ModelRenderer spiderRightArm2;
    public PMM_ModelRenderer spiderLeftArm2;
    public PMM_ModelRenderer spiderRightLeg2;
    public PMM_ModelRenderer spiderLeftLeg2;
    
	public PMM_ModelRenderer spiderLeftArmwear2;
	public PMM_ModelRenderer spiderRightArmwear2;
	public PMM_ModelRenderer spiderLeftLegwear2;
	public PMM_ModelRenderer spiderRightLegwear2;
	
	public boolean entityIsBesideClimbableBlock;

    public PMM_ModelSpider(float modelSize)
    {
        super(modelSize, 0.0F, 64, 64);
        this.modelScale = 0.75F;
        this.boobHeight = 0.5F;

        this.spiderLeftArm2 = new PMM_ModelRenderer(this, 32, 48);
        this.spiderLeftArm2.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
        this.spiderLeftArm2.setRotationPoint(5.0F, 2.5F + 0, 0.0F);
        this.spiderRightArm2 = new PMM_ModelRenderer(this, 40, 16);
        this.spiderRightArm2.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize);
        this.spiderRightArm2.setRotationPoint(-5.0F, 2.5F + 0, 0.0F);
        this.spiderLeftArmwear2 = new PMM_ModelRenderer(this, 48, 48);
        this.spiderLeftArmwear2.addBox(-1.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);
        this.spiderRightArmwear2 = new PMM_ModelRenderer(this, 40, 32);
        this.spiderRightArmwear2.addBox(-2.0F, -2.0F, -2.0F, 3, 12, 4, modelSize + 0.25F);

        this.spiderLeftLeg2 = new PMM_ModelRenderer(this, 16, 48);
        this.spiderLeftLeg2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
        this.spiderLeftLeg2.setRotationPoint(2.2F, 12.0F + 0, 0.0F);
        this.spiderRightLeg2 = new PMM_ModelRenderer(this, 0, 16);
        this.spiderRightLeg2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize);
        this.spiderRightLeg2.setRotationPoint(-2.2F, 12.0F + 0, 0.0F);
        this.spiderRightLegwear2 = new PMM_ModelRenderer(this, 0, 32);
        this.spiderRightLegwear2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
        this.spiderLeftLegwear2 = new PMM_ModelRenderer(this, 0, 48);
        this.spiderLeftLegwear2.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, modelSize + 0.25F);
        
		this.bipedBody.addChild(this.spiderRightArm2);
		this.bipedBody.addChild(this.spiderLeftArm2);
		this.bipedBody.addChild(this.spiderRightLeg2);
		this.bipedBody.addChild(this.spiderLeftLeg2);

		this.spiderLeftArm2.addChild (this.spiderLeftArmwear2);
		this.spiderRightArm2.addChild(this.spiderRightArmwear2);
		this.spiderLeftLeg2.addChild (this.spiderLeftLegwear2);
		this.spiderRightLeg2.addChild(this.spiderRightLegwear2);
		
		this.isSneak = true;
    }
    
    @Override
    protected void setJumpAnimations()
    {
    	if (this.entityIsBesideClimbableBlock)
    	{
    		this.limbSwing = this.ageInTicks / 0.5F;
    		this.limbSwingAmount = 0.7F;
    		this.setWalkingAnimations();
    	} else
    	{
    		super.setJumpAnimations();
    	}
    }
    
    @Override
    protected void setWalkingAnimations()
    {
    	super.setWalkingAnimations();
    	
		this.spiderRightArm2.rotateAngleX += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 1/2) * 2.0F * this.limbSwingAmount * 0.5F;
		this.spiderLeftArm2.rotateAngleX  += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 3/2) * 2.0F * this.limbSwingAmount * 0.5F;
		
		this.bipedLeftLeg.rotateAngleX     = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 0/2) * 1.4F * this.limbSwingAmount;
		this.bipedRightLeg.rotateAngleX    = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 1/2) * 1.4F * this.limbSwingAmount;
		this.spiderLeftLeg2.rotateAngleX  += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 2/2) * 1.4F * this.limbSwingAmount;
		this.spiderRightLeg2.rotateAngleX += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 3/2) * 1.4F * this.limbSwingAmount;
		
		this.bipedLeftArm.rotateAngleX     = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 3/2) * 1.4F * this.limbSwingAmount;
		this.bipedRightArm.rotateAngleX    = MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 2/2) * 1.4F * this.limbSwingAmount;
		this.spiderLeftArm2.rotateAngleX  += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 1/2) * 1.4F * this.limbSwingAmount;
		this.spiderRightArm2.rotateAngleX += MathHelper.cos(this.limbSwing * 1.3324F + (float)Math.PI * 0/2) * 1.4F * this.limbSwingAmount;
		
		this.bipedLeftLeg.rotationPointZ    += ((float)Math.sin(this.spiderRightLeg2.rotateAngleX + this.bipedBody.rotateAngleX) - 1.0F) * 1F;
		this.bipedRightLeg.rotationPointZ   += ((float)Math.sin(this.bipedLeftLeg.rotateAngleX + this.bipedBody.rotateAngleX) - 1.0F)    * 1F;
		this.spiderLeftLeg2.rotationPointZ  += ((float)Math.sin(this.bipedRightLeg.rotateAngleX + this.bipedBody.rotateAngleX) + 1.0F)   * 1F;
		this.spiderRightLeg2.rotationPointZ += ((float)Math.sin(this.spiderLeftLeg2.rotateAngleX + this.bipedBody.rotateAngleX) + 1.0F)  * 1F;
	
		this.bipedLeftArm.rotationPointZ    += ((float)Math.sin(this.spiderRightArm2.rotateAngleX + this.bipedBody.rotateAngleX) + 1.0F) * 1F;
		this.bipedRightArm.rotationPointZ   += ((float)Math.sin(this.bipedLeftArm.rotateAngleX + this.bipedBody.rotateAngleX) + 1.0F)    * 1F;
		this.spiderLeftArm2.rotationPointZ  += ((float)Math.sin(this.bipedRightArm.rotateAngleX + this.bipedBody.rotateAngleX) - 1.0F)   * 1F;
		this.spiderRightArm2.rotationPointZ += ((float)Math.sin(this.spiderLeftArm2.rotateAngleX + this.bipedBody.rotateAngleX) - 1.0F)  * 1F;
    }
    
    @Override
    protected void setSneakAnimations() {
    	super.setSneakAnimations();
        this.spiderRightLeg2.rotateAngleX -= 0.35F;
        this.spiderLeftLeg2.rotateAngleX  -= 0.35F;
    }
    
    @Override
    protected void setAddAnimations() {
    	this.bipedLeftLeg.rotateAngleX  -= 0.1F;
    	this.bipedRightLeg.rotateAngleX -= 0.1F;
    	this.bipedLeftLeg.rotateAngleY  -= 0.2F;
    	this.bipedRightLeg.rotateAngleY += 0.2F;
    	this.bipedLeftLeg.rotateAngleZ  += 0.1F;
    	this.bipedRightLeg.rotateAngleZ -= 0.1F;
    	
		this.spiderRightArm2.rotateAngleZ +=  0.5F;
		this.spiderLeftArm2.rotateAngleZ  += -0.5F;
		this.spiderRightArm2.rotateAngleY +=  0.5F;
		this.spiderLeftArm2.rotateAngleY  += -0.5F;
		this.spiderRightLeg2.rotateAngleZ +=  0.075F;
		this.spiderLeftLeg2.rotateAngleZ  += -0.075F;
		this.spiderRightLeg2.rotateAngleY +=  0.15F;
		this.spiderLeftLeg2.rotateAngleY  += -0.15F;

		this.spiderLeftLeg2.rotationPointX  += 1;
		this.spiderRightLeg2.rotationPointX += -1;

		float gapLY = 0.0F, gapRY = 0.0F;
		if (this.spiderLeftLeg2.rotateAngleX < 0.0F)
		{
			this.spiderLeftLeg2.rotationPointY += gapLY = (float)Math.sin(this.spiderLeftLeg2.rotateAngleX) * 2.0F;
			this.spiderLeftLeg2.rotationPointZ += (1F - (float)Math.cos(this.spiderLeftLeg2.rotateAngleX)) * 2.0F;
		}
		else
		{
			// 0.523599rad '=, 30deg
			this.spiderLeftLeg2.rotationPointY += gapLY = (float)Math.sin(-Math.min(this.spiderLeftLeg2.rotateAngleX, 0.523599D)) * 2.0F;
			this.spiderLeftLeg2.rotationPointZ -= (1F - (float)Math.cos(-Math.min(this.spiderLeftLeg2.rotateAngleX, 0.523599D))) * 2.0F;
		}
		if (this.spiderRightLeg2.rotateAngleX < 0.0F)
		{
			this.spiderRightLeg2.rotationPointY += gapRY = (float)Math.sin(this.spiderRightLeg2.rotateAngleX) * 2.0F;
			this.spiderRightLeg2.rotationPointZ += (1F - (float)Math.cos(this.spiderRightLeg2.rotateAngleX)) * 2.0F;
		}
		else
		{
			this.spiderRightLeg2.rotationPointY += gapRY = (float)Math.sin(-Math.min(this.spiderRightLeg2.rotateAngleX, 0.523599D)) * 2.0F;
			this.spiderRightLeg2.rotationPointZ -= (1F - (float)Math.cos(-Math.min(this.spiderRightLeg2.rotateAngleX, 0.523599D))) * 2.0F;
		}
    }
    
    
}
