package com.dodoneko.personificationmobmod.client.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.dodoneko.personificationmobmod.util.math.PMM_Math;
import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PMM_ModelRenderer extends ModelRenderer
{
    public float initRotateAngleX = 0.0F;
    public float initRotateAngleY = 0.0F;
    public float initRotateAngleZ = 0.0F;
    
    public float initRotationPointX = 0.0F;
    public float initRotationPointY = 0.0F;
    public float initRotationPointZ = 0.0F;
    
    public PMM_ModelRenderer parent;
    public Map<Integer, Vector6> renderMap = new HashMap();
	private int displayList;
	private boolean compiled;

	public PMM_ModelRenderer(ModelBase model, String boxNameIn) {
		super(model, boxNameIn);
    	this.textureWidth = 64.0F;
    	this.textureHeight = 64.0F;
    	this.parent = null;
	}
    public PMM_ModelRenderer(ModelBase model)
    {
        this(model, (String)null);
    }
    public PMM_ModelRenderer(ModelBase model, int texOffX, int texOffY)
    {
        this(model);
        this.setTextureOffset(texOffX, texOffY);
    }
    public PMM_ModelRenderer(ModelBase model, boolean show)
    {
        super(model, (String)null);
        this.showModel = show;
        this.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
    	this.parent = null;
    }

//    /** Null of PMM_ModelRenderer */
//    public PMM_ModelRenderer() {
//    	super(null, 0, 0);
//    	this.textureWidth = 0.0F;
//    	this.textureHeight = 0.0F;
//    	this.showModel = false;
//    	this.cubeList = Lists.<ModelBox>newArrayList();
//    }
    
//    @Override
//    public void render(float scale)
//    {
//    	float rate = 0.5F;
//    	
//        this.rotateAngleX = PMM_Math.lerpRotation(this.prevRotateAngleX, this.rotateAngleX, rate);
//        this.rotateAngleY = PMM_Math.lerpRotation(this.prevRotateAngleY, this.rotateAngleY, rate);
//        this.rotateAngleZ = PMM_Math.lerpRotation(this.prevRotateAngleZ, this.rotateAngleZ, rate);
//        this.rotationPointX = PMM_Math.lerp(this.prevRotationPointX, this.rotationPointX, rate);
//        this.rotationPointY = PMM_Math.lerp(this.prevRotationPointY, this.rotationPointY, rate);
//        this.rotationPointZ = PMM_Math.lerp(this.prevRotationPointZ, this.rotationPointZ, rate);
//        
//    	super.render(scale);
//        this.rotationPointX;
//        this.rotationPointY;
//        this.rotationPointZ;
//        this.rotateAngleX;
//        this.rotateAngleY;
//        this.rotateAngleZ;
//    }
    
    public void reset()
    {
    	this.rotateAngleX = this.initRotateAngleX;
    	this.rotateAngleY = this.initRotateAngleY;
    	this.rotateAngleZ = this.initRotateAngleZ;
    	this.rotationPointX = this.initRotationPointX;
    	this.rotationPointY = this.initRotationPointY;
    	this.rotationPointZ = this.initRotationPointZ;
    }
    
    public void setRotateAngle(float rotateAngleXIn,float rotateAngleYIn, float rotateAngleZIn)
    {
    	this.initRotateAngleX = this.rotateAngleX = rotateAngleXIn;
    	this.initRotateAngleY = this.rotateAngleY = rotateAngleYIn;
    	this.initRotateAngleZ = this.rotateAngleZ = rotateAngleZIn;
    }
    
    @Override
	public void setRotationPoint(float rotationPointXIn, float rotationPointYIn, float rotationPointZIn)
    {
    	this.initRotationPointX = this.rotationPointX = rotationPointXIn;
    	this.initRotationPointY = this.rotationPointY = rotationPointYIn;
    	this.initRotationPointZ = this.rotationPointZ = rotationPointZIn;
    }
	
	public void copy(PMM_ModelRenderer src)
	{
    	this.rotateAngleX = src.rotateAngleX;
    	this.rotateAngleY = src.rotateAngleY;
    	this.rotateAngleZ = src.rotateAngleZ;
    	this.rotationPointX = src.rotationPointX;
    	this.rotationPointY = src.rotationPointY;
    	this.rotationPointZ = src.rotationPointZ;
	}
	
	public void copyAngle(PMM_ModelRenderer src)
	{
    	this.rotateAngleX = src.rotateAngleX;
    	this.rotateAngleY = src.rotateAngleY;
    	this.rotateAngleZ = src.rotateAngleZ;
	}
	
	public void copyPoint(PMM_ModelRenderer src)
	{
    	this.rotationPointX = src.rotationPointX;
    	this.rotationPointY = src.rotationPointY;
    	this.rotationPointZ = src.rotationPointZ;
	}
	
	public void copyDisplayed(PMM_ModelRenderer src)
	{
		this.isHidden = src.isHidden;
		this.showModel = src.showModel;
	}
	
	public void copyCompletly(PMM_ModelRenderer src)
	{
		this.isHidden = src.isHidden;
    	this.rotateAngleX = src.rotateAngleX;
    	this.rotateAngleY = src.rotateAngleY;
    	this.rotateAngleZ = src.rotateAngleZ;
    	this.rotationPointX = src.rotationPointX;
    	this.rotationPointY = src.rotationPointY;
    	this.rotationPointZ = src.rotationPointZ;
    	this.initRotateAngleX = src.initRotateAngleX;
    	this.initRotateAngleY = src.initRotateAngleY;
    	this.initRotateAngleZ = src.initRotateAngleZ;
    	this.initRotationPointX = src.initRotationPointX;
    	this.initRotationPointY = src.initRotationPointY;
    	this.initRotationPointZ = src.initRotationPointZ;
		this.showModel = src.showModel;
	}
	
	public void render(float scale, int entityHash) {
		Vector6 prevData;

        if (!this.isHidden)
        {
            if (this.showModel)
            {
        		
        		if (!this.renderMap.containsKey(entityHash))
        		{
        			this.renderMap.put(entityHash, new Vector6(this));
        		}
        		prevData = this.renderMap.get(entityHash);
        		
        		float speed = 0.5F;
        		this.rotateAngleX = PMM_Math.lerpRotation(prevData.rx, this.rotateAngleX, speed);
        		this.rotateAngleY = PMM_Math.lerpRotation(prevData.ry, this.rotateAngleY, speed);
        		this.rotateAngleZ = PMM_Math.lerpRotation(prevData.rz, this.rotateAngleZ, speed);
        		this.rotationPointX = PMM_Math.lerp(prevData.px, this.rotationPointX, speed);
        		this.rotationPointY = PMM_Math.lerp(prevData.py, this.rotationPointY, speed);
        		this.rotationPointZ = PMM_Math.lerp(prevData.pz, this.rotationPointZ, speed);
        		
                if (!this.compiled)
                {
                    this.compileDisplayList(scale);
                }

                GlStateManager.translate(this.offsetX, this.offsetY, this.offsetZ);

                if (this.rotateAngleX == 0.0F && this.rotateAngleY == 0.0F && this.rotateAngleZ == 0.0F)
                {
                    if (this.rotationPointX == 0.0F && this.rotationPointY == 0.0F && this.rotationPointZ == 0.0F)
                    {
                        GlStateManager.callList(this.displayList);

                        if (this.childModels != null)
                        {
                            for (int k = 0; k < this.childModels.size(); ++k)
                            {
                                ((PMM_ModelRenderer)this.childModels.get(k)).render(scale, entityHash);
                            }
                        }
                    }
                    else
                    {
                        GlStateManager.translate(this.rotationPointX * scale, this.rotationPointY * scale, this.rotationPointZ * scale);
                        GlStateManager.callList(this.displayList);

                        if (this.childModels != null)
                        {
                            for (int j = 0; j < this.childModels.size(); ++j)
                            {
                                ((PMM_ModelRenderer)this.childModels.get(j)).render(scale, entityHash);
                            }
                        }

                        GlStateManager.translate(-this.rotationPointX * scale, -this.rotationPointY * scale, -this.rotationPointZ * scale);
                    }
                }
                else
                {
                    GlStateManager.pushMatrix();
                    GlStateManager.translate(this.rotationPointX * scale, this.rotationPointY * scale, this.rotationPointZ * scale);

                    if (this.rotateAngleZ != 0.0F)
                    {
                        GlStateManager.rotate(this.rotateAngleZ * (180F / (float)Math.PI), 0.0F, 0.0F, 1.0F);
                    }

                    if (this.rotateAngleY != 0.0F)
                    {
                        GlStateManager.rotate(this.rotateAngleY * (180F / (float)Math.PI), 0.0F, 1.0F, 0.0F);
                    }

                    if (this.rotateAngleX != 0.0F)
                    {
                        GlStateManager.rotate(this.rotateAngleX * (180F / (float)Math.PI), 1.0F, 0.0F, 0.0F);
                    }

                    GlStateManager.callList(this.displayList);

                    if (this.childModels != null)
                    {
                        for (int i = 0; i < this.childModels.size(); ++i)
                        {
                            ((PMM_ModelRenderer)this.childModels.get(i)).render(scale, entityHash);
                        }
                    }

                    GlStateManager.popMatrix();
                }

                GlStateManager.translate(-this.offsetX, -this.offsetY, -this.offsetZ);

        		this.renderMap.get(entityHash).set(this).changedTime = Minecraft.getSystemTime();
            }
        }
	}
	

    @SideOnly(Side.CLIENT)
    private void compileDisplayList(float scale)
    {
        this.displayList = GLAllocation.generateDisplayLists(1);
        GlStateManager.glNewList(this.displayList, 4864);
        VertexBuffer vertexbuffer = Tessellator.getInstance().getBuffer();

        for (int i = 0; i < this.cubeList.size(); ++i)
        {
            ((ModelBox)this.cubeList.get(i)).render(vertexbuffer, scale);
        }

        GlStateManager.glEndList();
        this.compiled = true;
    }
	
	@Override
	public PMM_ModelRenderer setTextureSize(int textureWidthIn, int textureHeightIn)
	{
		return (PMM_ModelRenderer)super.setTextureSize(textureWidthIn, textureHeightIn);
	}

	@Override
	public void addChild(ModelRenderer renderer) {
//		if (renderer.parent  != null)
//		{
//			if (renderer.parent.childModels.contains(renderer))
//				renderer.parent.childModels.remove(renderer);
//		}
		((PMM_ModelRenderer)renderer).parent = this;
		super.addChild((ModelRenderer)renderer);
	}
	
	
	private class Vector6
	{
		public float px, py, pz, rx, ry, rz;
		public long changedTime;
		
		public Vector6(){}
		public Vector6(float pxIn, float pyIn, float pzIn, float rxIn, float ryIn, float rzIn)
		{
			this.px = pxIn;
			this.py = pyIn;
			this.pz = pzIn;
			this.rx = rxIn;
			this.ry = ryIn;
			this.rz = rzIn;
		}
		public Vector6(PMM_ModelRenderer renderer)
		{
			this.px = renderer.rotationPointX;
			this.py = renderer.rotationPointY;
			this.pz = renderer.rotationPointZ;
			this.rx = renderer.rotateAngleX;
			this.ry = renderer.rotateAngleY;
			this.rz = renderer.rotateAngleZ;
		}
		
		public Vector6 set(PMM_ModelRenderer renderer)
		{
			this.px = renderer.rotationPointX;
			this.py = renderer.rotationPointY;
			this.pz = renderer.rotationPointZ;
			this.rx = renderer.rotateAngleX;
			this.ry = renderer.rotateAngleY;
			this.rz = renderer.rotateAngleZ;
			return this;
		}
	}
}
