package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.passive.EntitySheep;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelSheepFar extends PMM_ModelSheep
{
    public PMM_ModelSheepFar()
    {
        super(0.125F);
        this.bipedBody.showModel     = false;
        this.bipedLeftArm.showModel  = false;
        this.bipedRightArm.showModel = false;
        this.bipedLeftLeg.showModel  = false;
        this.bipedRightLeg.showModel = false;
        this.modelScale = super.modelScale;
        this.boobHeight = super.boobHeight;
    }
}