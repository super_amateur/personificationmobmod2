package com.dodoneko.personificationmobmod.client.model;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_ModelChicken extends PMM_ModelBiped
{
	// TODO: しっぽつける
    public PMM_ModelChicken()
    {
    	super();
    	this.modelScale = 0.45F;
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setAddAnimations(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
//        this.bill.rotateAngleX = this.head.rotateAngleX;
//        this.bill.rotateAngleY = this.head.rotateAngleY;
//        this.chin.rotateAngleX = this.head.rotateAngleX;
//        this.chin.rotateAngleY = this.head.rotateAngleY;
//        this.bipedBody.rotateAngleX = ((float)Math.PI / 2F);
        this.bipedRightLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.bipedLeftLeg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
        this.bipedRightArm.rotateAngleZ = ageInTicks;
        this.bipedLeftArm.rotateAngleZ = -ageInTicks;
    }
}