package com.dodoneko.personificationmobmod.client.renderer.entity.layers;

import net.minecraft.client.renderer.entity.layers.LayerRenderer;

import com.dodoneko.personificationmobmod.client.model.PMM_ModelPig;
import com.dodoneko.personificationmobmod.client.renderer.entity.PMM_RenderPig;

import net.minecraft.entity.passive.EntityPig;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_LayerSaddle implements LayerRenderer<EntityPig>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/pig/pig_saddle.png");
    private final PMM_RenderPig pigRenderer;
    private final PMM_ModelPig pigModel = new PMM_ModelPig(0.5F);

    public PMM_LayerSaddle(PMM_RenderPig pmm_RenderPig)
    {
        this.pigRenderer = pmm_RenderPig;
    }

    @Override
	public void doRenderLayer(EntityPig entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        if (entitylivingbaseIn.getSaddled())
        {
            this.pigRenderer.bindTexture(TEXTURE);
            this.pigModel.setModelAttributes(this.pigRenderer.getMainModel());
            this.pigModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
        }
    }

    @Override
	public boolean shouldCombineTextures()
    {
        return false;
    }
}