package com.dodoneko.personificationmobmod.client.renderer.entity;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelCow;

import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderCow extends RenderLiving<EntityCow>
{
	private static final ResourceLocation COW_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
//  private static final ResourceLocation COW_TEXTURES = new ResourceLocation("textures/entity/cow/cow.png");

    public PMM_RenderCow(RenderManager renderManagerIn)
    {
    	super(renderManagerIn, new PMM_ModelCow(), 0.7F);
    }
    
    @Override
    public void doRender(EntityCow entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelCow model = (PMM_ModelCow)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityCow entity)
    {
        return COW_TEXTURES;
    }
}