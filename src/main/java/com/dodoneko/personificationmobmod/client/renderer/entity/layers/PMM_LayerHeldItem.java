package com.dodoneko.personificationmobmod.client.renderer.entity.layers;

import net.minecraft.client.renderer.entity.layers.LayerRenderer;

import java.util.List;

import com.dodoneko.personificationmobmod.client.model.PMM_ModelBiped;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelRenderer;
import com.dodoneko.personificationmobmod.util.math.PMM_Math;
import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_LayerHeldItem implements LayerRenderer<EntityLivingBase>
{
    protected final RenderLivingBase<?> livingEntityRenderer;
    protected PMM_ModelBiped model;

    public PMM_LayerHeldItem(RenderLivingBase<?> livingEntityRendererIn)
    {
        this.livingEntityRenderer = livingEntityRendererIn;
    }

    @Override
	public void doRenderLayer(EntityLivingBase entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
    	this.model = (PMM_ModelBiped) this.livingEntityRenderer.getMainModel();
        boolean flag = entitylivingbaseIn.getPrimaryHand() == EnumHandSide.RIGHT;
        ItemStack itemstack = flag ? entitylivingbaseIn.getHeldItemOffhand() : entitylivingbaseIn.getHeldItemMainhand();
        ItemStack itemstack1 = flag ? entitylivingbaseIn.getHeldItemMainhand() : entitylivingbaseIn.getHeldItemOffhand();

        if (!itemstack.isEmpty() || !itemstack1.isEmpty())
        {
            GlStateManager.pushMatrix();

//            if (this.model.isChild)
//            {
//                float f = 0.5F;
//                GlStateManager.translate(0.0F, 0.75F, 0.0F);
//                GlStateManager.scale(0.5F, 0.5F, 0.5F);
//            }

            this.renderHeldItem(entitylivingbaseIn, itemstack1, ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, EnumHandSide.RIGHT);
            this.renderHeldItem(entitylivingbaseIn, itemstack, ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND, EnumHandSide.LEFT);
            GlStateManager.popMatrix();
        }
    }

    private void renderHeldItem(EntityLivingBase p_188358_1_, ItemStack p_188358_2_, ItemCameraTransforms.TransformType p_188358_3_, EnumHandSide handSide)
    {
        if (!p_188358_2_.isEmpty())
        {
            GlStateManager.pushMatrix();

            boolean flag = handSide == EnumHandSide.LEFT;
            PMM_ModelRenderer renderer = flag? model.bipedLeftArm: model.bipedRightArm;
            translateInGlobal(renderer);
            GlStateManager.translate(1.25F * (flag?-1:1) * this.model.scaleFactor * this.model.modelScale, 10.0F * this.model.scaleFactor * this.model.modelScale, -2.0F * this.model.scaleFactor * this.model.modelScale);
            // Forge: moved this call down, fixes incorrect offset while sneaking.
//            this.translateToHand(handSide);
            GlStateManager.rotate(-90.0F, 1.0F, 0.0F, 0.0F);
            GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
            if (this.model.isChild)
            	GlStateManager.scale(0.5F, 0.5F, 0.5F);
            
            Minecraft.getMinecraft().getItemRenderer().renderItemSide(p_188358_1_, p_188358_2_, p_188358_3_, flag);
            
            GlStateManager.popMatrix();
        }
    }

    private void translateInGlobal(PMM_ModelRenderer renderer) {
		if (renderer.parent != null && renderer != this.model.bipedBody && renderer != this.model.bipedHead)
		{
//			translateInGlobal(renderer);
		}
		
		GlStateManager.translate(renderer.rotationPointX * this.model.scaleFactor * this.model.modelScale, renderer.rotationPointY * this.model.scaleFactor * this.model.modelScale, renderer.rotationPointZ * this.model.scaleFactor * this.model.modelScale);
        if (renderer.rotateAngleZ != 0.0F)
        {
            GlStateManager.rotate(renderer.rotateAngleZ * PMM_Math.Rad2Deg, 0.0F, 0.0F, 1.0F);
        }

        if (renderer.rotateAngleY != 0.0F)
        {
            GlStateManager.rotate(renderer.rotateAngleY * PMM_Math.Rad2Deg, 0.0F, 1.0F, 0.0F);
        }

        if (renderer.rotateAngleX != 0.0F)
        {
            GlStateManager.rotate(renderer.rotateAngleX * PMM_Math.Rad2Deg, 1.0F, 0.0F, 0.0F);
        }
	}

	protected void translateToHand(EnumHandSide p_191361_1_)
    {
//        ((PMM_ModelBiped)this.livingEntityRenderer.getMainModel()).postRenderArm(0.0625F, p_191361_1_);
    }

    @Override
	public boolean shouldCombineTextures()
    {
        return false;
    }
}