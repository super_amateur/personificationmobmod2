package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSquid;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelWolf;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerWolfCollar;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderWolf extends RenderLiving<EntityWolf>
{
//    private static final ResourceLocation WOLF_TEXTURES       = new ResourceLocation("textures/entity/wolf/wolf.png");
//    private static final ResourceLocation TAMED_WOLF_TEXTURES = new ResourceLocation("textures/entity/wolf/wolf_tame.png");
//    private static final ResourceLocation ANRGY_WOLF_TEXTURES = new ResourceLocation("textures/entity/wolf/wolf_angry.png");
    private static final ResourceLocation WOLF_TEXTURES       = new ResourceLocation("textures/entity/sample-chan.png");
    private static final ResourceLocation TAMED_WOLF_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
    private static final ResourceLocation ANRGY_WOLF_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");

    public PMM_RenderWolf(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelWolf(), 0.5F);
        this.addLayer(new PMM_LayerWolfCollar(this));
    }

    /**
     * Defines what float the third param in setRotationAngles of ModelBase is
     */
    @Override
	protected float handleRotationFloat(EntityWolf livingBase, float partialTicks)
    {
        return livingBase.getTailRotation();
    }
    
    @Override
    public void doRender(EntityWolf entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelWolf model = (PMM_ModelWolf)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
        if (entity.isWolfWet())
        {
            float f = entity.getBrightness(partialTicks) * entity.getShadingWhileWet(partialTicks);
            GlStateManager.color(f, f, f);
        }

        super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityWolf entity)
    {
        return entity.isTamed() ? TAMED_WOLF_TEXTURES : (entity.isAngry() ? ANRGY_WOLF_TEXTURES : WOLF_TEXTURES);
    }
}