package com.dodoneko.personificationmobmod.client.renderer.entity.layers;

import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.model.ModelZombieVillager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_LayerVillagerArmor extends LayerBipedArmor
{
    public PMM_LayerVillagerArmor(RenderLivingBase<?> rendererIn)
    {
        super(rendererIn);
    }

    @Override
	protected void initArmor()
    {
        this.modelLeggings = new ModelZombieVillager(0.5F, 0.0F, true);
        this.modelArmor = new ModelZombieVillager(1.0F, 0.0F, true);
    }
}