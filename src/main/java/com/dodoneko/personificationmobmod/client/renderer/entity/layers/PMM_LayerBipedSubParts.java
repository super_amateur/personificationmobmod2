package com.dodoneko.personificationmobmod.client.renderer.entity.layers;

import com.dodoneko.personificationmobmod.client.model.PMM_ModelBiped;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBipedSubParts;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSheepFar;
import com.dodoneko.personificationmobmod.client.renderer.entity.PMM_RenderSheep;

import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

public class PMM_LayerBipedSubParts<T extends RenderLivingBase, U extends PMM_ModelBiped> implements LayerRenderer<EntityLivingBase>
{
    private static ResourceLocation TEXTURE;
    public final T baseRenderer;
    private final PMM_ModelBipedSubParts partsModel;
    
	public PMM_LayerBipedSubParts(T renderer, ResourceLocation texture)
	{
		this.TEXTURE = texture;
		this.baseRenderer = renderer;
		this.partsModel = new PMM_ModelBipedSubParts<U>(this, 0.0F);
	}

	@Override
	public void doRenderLayer(EntityLivingBase entitylivingbaseIn, float limbSwing, float limbSwingAmount,
			float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		
		if (true)
		{
			baseRenderer.bindTexture(TEXTURE);

            this.partsModel.setModelAttributes((U)this.baseRenderer.getMainModel());
            this.partsModel.render(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		}
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

}
