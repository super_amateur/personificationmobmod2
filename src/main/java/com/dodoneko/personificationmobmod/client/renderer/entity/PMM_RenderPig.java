package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelPig;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerSaddle;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderPig extends RenderLiving<EntityPig>
{
	private static final ResourceLocation PIG_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
//  private static final ResourceLocation PIG_TEXTURES = new ResourceLocation("textures/entity/pig/pig.png");

    public PMM_RenderPig(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelPig(), 0.7F);
        this.addLayer(new PMM_LayerSaddle(this));
    }
    
    @Override
    public void doRender(EntityPig entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelPig model = (PMM_ModelPig)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityPig entity)
    {
        return PIG_TEXTURES;
    }
}