package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSquid;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelZombie;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerHeldItem;

import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderZombie extends RenderBiped<EntityZombie>
{
//    private static final ResourceLocation ZOMBIE_TEXTURES = new ResourceLocation("textures/entity/zombie/zombie.png");
    private static final ResourceLocation ZOMBIE_TEXTURES = new ResourceLocation("textures/entity/zombie/zombie-chan.png");

    public PMM_RenderZombie(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelZombie(), 0.5F);
        this.layerRenderers.remove(2); // Remove LayerHeldItem.
        this.addLayer(new PMM_LayerHeldItem(this));
        LayerBipedArmor layerbipedarmor = new LayerBipedArmor(this)
        {
            @Override
			protected void initArmor()
            {
                this.modelLeggings = new PMM_ModelZombie(0.5F, true);
                this.modelArmor = new PMM_ModelZombie(1.0F, true);
            }
        };
        this.addLayer(layerbipedarmor);
    }
    
    @Override
    public void doRender(EntityZombie entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelZombie model = (PMM_ModelZombie)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityZombie entity)
    {
        return ZOMBIE_TEXTURES;
    }
}