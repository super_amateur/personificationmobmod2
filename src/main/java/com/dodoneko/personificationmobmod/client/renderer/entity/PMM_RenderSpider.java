package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSpider;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerSpiderEyes;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderSpider<T extends EntitySpider> extends RenderLiving<T>
{
    private static final ResourceLocation SPIDER_TEXTURES = new ResourceLocation("textures/entity/spider/spider-chan.png");

    public PMM_RenderSpider(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelSpider(0.0F), 1.0F);
        this.addLayer(new PMM_LayerSpiderEyes(this));
    }

	@Override
    public void doRender(T entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelSpider model = (PMM_ModelSpider)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	model.entityIsBesideClimbableBlock = entity.isBesideClimbableBlock();
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    @Override
	protected float getDeathMaxRotation(T entityLivingBaseIn)
    {
        return 180.0F;
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(T entity)
    {
        return SPIDER_TEXTURES;
    }
}