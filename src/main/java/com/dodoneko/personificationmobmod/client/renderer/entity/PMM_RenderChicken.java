package com.dodoneko.personificationmobmod.client.renderer.entity;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelChicken;

import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderChicken extends RenderLiving<EntityChicken>
{
//	private static final ResourceLocation CHICKEN_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
    private static final ResourceLocation CHICKEN_TEXTURES = new ResourceLocation("textures/entity/chicken-chan.png");

    public PMM_RenderChicken(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelChicken(), 0.3F);
    }
    
    @Override
    public void doRender(EntityChicken entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelChicken model = (PMM_ModelChicken)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityChicken entity)
    {
        return CHICKEN_TEXTURES;
    }

    /**
     * Defines what float the third param in setRotationAngles of ModelBase is
     */
    @Override
	protected float handleRotationFloat(EntityChicken livingBase, float partialTicks)
    {
        float f = livingBase.oFlap + (livingBase.wingRotation - livingBase.oFlap) * partialTicks;
        float f1 = livingBase.oFlapSpeed + (livingBase.destPos - livingBase.oFlapSpeed) * partialTicks;
        return (MathHelper.sin(f) + 1.0F) * f1;
    }
}