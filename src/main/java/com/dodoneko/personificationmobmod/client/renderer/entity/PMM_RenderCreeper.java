package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBiped;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelCreeper;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerBipedSubParts;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerCreeperCharge;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderCreeper extends RenderLiving<EntityCreeper>
{
//    private static final ResourceLocation CREEPER_TEXTURES = new ResourceLocation("textures/entity/creeper/creeper.png");
    private static final ResourceLocation CREEPER_TEXTURES = new ResourceLocation("textures/entity/creeper/creeper-chan.png");
//    private static final ResourceLocation CREEPER_SUB_TEXTURES = new ResourceLocation("textures/entity/sample-map.png");
    public PMM_RenderCreeper(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelCreeper(0.0f), 0.5F);
//        super(renderManagerIn, new ModelPlayer(0.8F, true), 0.5F);
        this.addLayer(new PMM_LayerCreeperCharge(this));
//        this.addLayer(new PMM_LayerBipedSubParts<PMM_RenderCreeper, PMM_ModelCreeper>(this, CREEPER_SUB_TEXTURES));
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityCreeper entitylivingbaseIn, float partialTickTime)
    {
        float f = entitylivingbaseIn.getCreeperFlashIntensity(partialTickTime);
        float f1 = 1.0F + MathHelper.sin(f * 100.0F) * f * 0.01F;
        f = MathHelper.clamp(f, 0.0F, 1.0F);
        f = f * f;
        f = f * f;
        float f2 = (1.0F + f * 0.4F) * f1;
        float f3 = (1.0F + f * 0.1F) / f1;
        GlStateManager.scale(f2, f3, f2);
    }
    
    @Override
    public void doRender(EntityCreeper entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelCreeper model = (PMM_ModelCreeper)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Gets an RGBA int color multiplier to apply.
     */
    @Override
	protected int getColorMultiplier(EntityCreeper entitylivingbaseIn, float lightBrightness, float partialTickTime)
    {
        float f = entitylivingbaseIn.getCreeperFlashIntensity(partialTickTime);

        if ((int)(f * 10.0F) % 2 == 0)
        {
            return 0;
        }
        else
        {
            int i = (int)(f * 0.2F * 255.0F);
            i = MathHelper.clamp(i, 0, 255);
            return i << 24 | 822083583;
        }
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityCreeper entity)
    {
        return CREEPER_TEXTURES;
    }
}