package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.client.model.PMM_ModelSkeleton;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerHeldItem;

import net.minecraft.client.model.ModelSkeleton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.entity.monster.AbstractSkeleton;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderSkeleton extends RenderBiped<AbstractSkeleton>
{
    private static final ResourceLocation SKELETON_TEXTURES = new ResourceLocation("textures/entity/skeleton/skeleton-chan.png");

    public PMM_RenderSkeleton(RenderManager renderManagerIn)
    {
    	this(renderManagerIn, false);
    }
    public PMM_RenderSkeleton(RenderManager renderManagerIn, boolean isWither)
    {
        super(renderManagerIn, new PMM_ModelSkeleton(), 0.5F);
        this.layerRenderers.remove(2); // remove LayerHeldItem.
        this.addLayer(new PMM_LayerHeldItem(this));
        this.addLayer(new LayerBipedArmor(this)
        {
            protected void initArmor()
            {
                this.modelLeggings = new ModelSkeleton(0.5F, true);
                this.modelArmor = new ModelSkeleton(1.0F, true);
            }
        });
    }
    
    @Override
    public void doRender(AbstractSkeleton entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelSkeleton model = (PMM_ModelSkeleton)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	model.entityHeldItemInMainHand = entity.getHeldItem(EnumHand.MAIN_HAND);
    	model.entityPrimaryHand = entity.getPrimaryHand();
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    public void transformHeldFull3DItemLayer()
    {
        GlStateManager.translate(0.09375F, 0.1875F, 0.0F);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(AbstractSkeleton entity)
    {
        return SKELETON_TEXTURES;
    }
}