package com.dodoneko.personificationmobmod.client.renderer.entity;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSkeleton;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.monster.AbstractSkeleton;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderBat extends RenderLiving<EntityBat>
{
    private static final ResourceLocation BAT_TEXTURES = new ResourceLocation("textures/entity/bat-chan.png");

    public PMM_RenderBat(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelBat(), 0.25F);
    }

	/**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntityBat entity)
    {
        return BAT_TEXTURES;
    }
    
    @Override
    public void doRender(EntityBat entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelBat model = (PMM_ModelBat)this.getMainModel();
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	model.entityIsBatHanging = entity.getIsBatHanging();
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Allows the render to do state modifications necessary before the model is rendered.
     */
    @Override
	protected void preRenderCallback(EntityBat entitylivingbaseIn, float partialTickTime)
    {
        GlStateManager.scale(0.35F, 0.35F, 0.35F);
    }

    @Override
	protected void applyRotations(EntityBat entityLiving, float p_77043_2_, float p_77043_3_, float partialTicks)
    {
        if (entityLiving.getIsBatHanging())
        {
            GlStateManager.translate(0.0F, -0.1F, 0.0F);
        }
        else
        {
        	// TODO: ここの記述消して、PMM_ModelBatで腕の動きに合わせて上下させる
            GlStateManager.translate(0.0F, MathHelper.cos(p_77043_2_ * 0.3F) * 0.1F, 0.0F);
        }

        super.applyRotations(entityLiving, p_77043_2_, p_77043_3_, partialTicks);
    }
}