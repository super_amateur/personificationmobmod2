package com.dodoneko.personificationmobmod.client.renderer.entity;

import com.dodoneko.personificationmobmod.PersonificationMobMod;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelBat;
import com.dodoneko.personificationmobmod.client.model.PMM_ModelSheep;
import com.dodoneko.personificationmobmod.client.renderer.entity.layers.PMM_LayerSheepWool;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class PMM_RenderSheep extends RenderLiving<EntitySheep>
{
	private static final ResourceLocation SHEARED_SHEEP_TEXTURES = new ResourceLocation("textures/entity/sample-chan.png");
//  private static final ResourceLocation SHEARED_SHEEP_TEXTURES = new ResourceLocation("textures/entity/sheep/sheep.png");

    public PMM_RenderSheep(RenderManager renderManagerIn)
    {
        super(renderManagerIn, new PMM_ModelSheep(), 0.7F);
        this.addLayer(new PMM_LayerSheepWool(this));
    }
    
    @Override
    public void doRender(EntitySheep entity, double x, double y, double z, float entityYaw, float partialTicks)
    {
    	PMM_ModelSheep model = (PMM_ModelSheep)this.mainModel;
    	model.entityId = entity.getEntityId();
    	model.entityMotionY = entity.motionY;
    	model.entityOnGround = entity.onGround;
    	model.entityIsInWater = entity.isInWater();
    	model.entityHurtTime = entity.hurtTime;
    	
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	protected ResourceLocation getEntityTexture(EntitySheep entity)
    {
        return SHEARED_SHEEP_TEXTURES;
    }
}