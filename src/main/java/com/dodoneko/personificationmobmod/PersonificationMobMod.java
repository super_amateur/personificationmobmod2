package com.dodoneko.personificationmobmod;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.dodoneko.personificationmobmod.client.renderer.entity.*;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.*;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
@Mod(modid = PersonificationMobMod.MODID, name = PersonificationMobMod.MODNAME, version = PersonificationMobMod.VERSION)
public class PersonificationMobMod
{
	public static final String MODID = "personificationmobmod";
	public static final String MODNAME = "Personification Mob Mod";
	public static final String VERSION = "1.11.2 1.0.0a.354";
	/*
	 * 1.11.2 1.0.0a.x - 
	 * 1.11.2 1.0.0a.354 - HashMapを利用して、Entityごとのprev情報を保存し、イーズモーションを実装した。
	 * 1.11.2 1.0.0a.352 - 付属パーツ実装。スカート2種。
	 * 1.11.2 1.0.0a.345 - 道具を持った時、手に追従するようにPMM_LayerHeldItemを調整。
	 * 1.11.2 1.0.0a.335 - スパイダーちゃんの動きを改善
	 * 1.11.2 1.0.0a.315 - どのEntityクラスかを変数で定義するのをやめ、setAnimations()内で使うパラメーターはすべてPMM_RenderXXXのdoRender()で記述することに。イーズモーションをやめることで、キャラごとのモーションを確率させることに成功。
	 * 1.11.2 1.0.0a.306 - PMM_ModelBipedのsetAnimations()の引数をPMM_ModelBipedのメンバ変数にした。どのEntityクラスかを変数で定義できるようにした。
	 * 1.11.2 1.0.0a.291 - コウモリちゃんの実装
	 * 17/04/02
	 * 1.11.2 1.0.0a.250 - ほとんぼのMobのRenderとModelを実装した。これから改善していく。
	 * 1.11.2 1.0.0a.236 - 洞窟スパイダーちゃんの実装
	 * - 17/03/30
	 * 1.11.2 1.0.0a.230 - スパイダーちゃん実装
	 * 1.11.2 1.0.0a.218 - 回転の動きにスムースをかけた。
	 * - 17/03/28
	 * 1.11.2 1.0.0a.216 - エンダーマンちゃんの、ブロック持ってる時、攻撃時のモーション追加。
	 * 1.11.2 1.0.0a.210 - PMM_ModelRendererの実装。prevRotationPointやprevRotateAngleなど
	 * - 17/03/21
	 * 1.11.2 1.0.0a.209 - エンダーマンちゃんのモーション調整
	 * 1.11.2 1.0.0a.207 - MPMM_ModelBipedで、各パーツの親子関係をModelRendererのChildrenで登録。
	 * 1.11.2 1.0.0a.198 - エンダーマンちゃん実装
	 * - 17/03/20
	 * 1.11.2 1.0.0a.175 - Bipedモーションの修正ンの修正
	 * 1.11.2 1.0.0a.165 - 空中で飛んでたり落ちてたりする時のモーションをかわいくした 
	 * - 17/03/11
	 * 1.11.2 1.0.0a.163 - ダメージ時のアニメーションをかわいくした。
	 * 1.11.2 1.0.0a.161 - ModelBipedを関数にめっちゃ分けて汎用性高めた。
	 * - 17/03/09
	 * 1.11.2 1.0.0a.153 - イカちゃん実装
	 * - 17/03/06
	 * 1.11.2 1.0.0a.138 - ウィザースケルトンちゃん実装
	 * - 17/03/05
	 * 1.11.2 1.0.0a.136 - スケルトンちゃん追加
	 * 1.11.2 1.0.0a.133 - 子どもモデルの修正
	 * 1.11.2 1.0.0a.100 - ゾンビちゃん追加
	 * 1.11.2 1.0.0a.99 - PMM_ModelBipedを、ModelBipedも継承するようにした。
	 * - 2017/03/03
	 * 1.11.2 1.0.0a.98 - 手足を広げるように振るようにして、歩くモーションをかわいくした。
	 * - 2017/02/28
	 * 1.11.2 1.0.0a.93 - まぶた、まばたきを実装
	 * 1.11.2 1.0.0a.79 - おっぱいの服、クリーパーちゃんのテクスチャを実装
	 * - 2017/02/27
	 *　1.11.2 1.0.0a.76 - おっぱい。（揺れる）
	 * 1.11.2 1.0.0a.49 - アホ毛の動きをかわいく演出
	 * 1.11.2 1.0.0a.39 - ModelBipedにアホ毛導入
	 * - 2017/02/26
	 * 1.11.2 1.0.0a.27 - モデルのサイズの調整をできるようにした
	 * 1.11.2 1.0.0a.7 - setEntityList関数を再び作成。Renderのセットだけ関数に入れて見やすくした。
	 * 1.11.2 1.0.0a.5 - Proxy消した。レンダラーの置き換えをHashMap使った書き方に書き直した。
	 * - 2017/02/25
	 * 1.11.2 1.0.0a.4 - Proxy無しでエンティティのレンダラー置き換えができるかテスト。
	 * 1.11.2 1.0.0a.2 - ClientProxy.javaにて、コンフィグ関連のプログラムを削除。
	 * 1.11.2 1.0.0a.1 - テクスチャのチェック。Rendererでテクスチャ名を~~~-chan.pngに変えるだけでリソース取ってくるかどうか？
	 * 1.11.2 1.0.0a.0 - ColerfulCreeperからのコピペ。Modding環境が正常かどうかのチェック。
	 * - 2017/02/24
	 */


	public static Map<Class<? extends EntityLivingBase>, RenderLivingBase> renderMap = new HashMap();
	@SideOnly(Side.CLIENT)
	private void setEntityList(RenderManager renderManagerIn)
	{
		//    	// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
		renderMap.put(EntityBat.class,            new PMM_RenderBat(renderManagerIn));
		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntityCow.class,            new PMM_RenderCow(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
		renderMap.put(EntityChicken.class,        new PMM_RenderChicken(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntityPig.class,            new PMM_RenderPig(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntitySheep.class,          new PMM_RenderSheep(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntitySlime.class,          new PMM_RenderSlime(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntityVillager.class,       new PMM_RenderVillager(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntityWitch.class,          new PMM_RenderWitch(renderManagerIn));
//		// last update - 1.11.2 1.0.0a.250 * add - 1.11.2 1.0.0a.250
//		renderMap.put(EntityWolf.class,           new PMM_RenderWolf(renderManagerIn));
		// last update - 1.11.2 1.0.0a.231 * add - 1.11.2 1.0.0a.230
		renderMap.put(EntityCaveSpider.class,     new PMM_RenderCaveSpider(renderManagerIn));
		// last update - 1.11.2 1.0.0a.218 * add - 1.11.2 1.0.0a.230
		renderMap.put(EntitySpider.class,         new PMM_RenderSpider(renderManagerIn));
		// last update - 1.11.2 1.0.0a.216 * add - 1.11.2 1.0.0a.175
		renderMap.put(EntityEnderman.class,       new PMM_RenderEnderman(renderManagerIn));
		// last update - 1.11.2 1.0.0a.139 * add - 1.11.2 1.0.0a.139
		renderMap.put(EntitySquid.class,          new PMM_RenderSquid(renderManagerIn));
		// last update - 1.11.2 1.0.0a.137 * add - 1.11.2 1.0.0a.137
		renderMap.put(EntityWitherSkeleton.class, new PMM_RenderWitherSkeleton(renderManagerIn));
		// last update - 1.11.2 1.0.0a.136 * add - 1.11.2 1.0.0a.134
		renderMap.put(EntitySkeleton.class,       new PMM_RenderSkeleton(renderManagerIn));
		// last update - 1.11.2 1.0.0a.100 * add - 1.11.2 1.0.0a.100
		renderMap.put(EntityZombie.class,         new PMM_RenderZombie(renderManagerIn));
		// last update - 1.11.2 1.0.0a.79  * add - 1.11.2 1.0.0a.5  
		renderMap.put(EntityCreeper.class,        new PMM_RenderCreeper(renderManagerIn));
	}



	public static RenderManager renderManager;

	@EventHandler
	@SideOnly(Side.CLIENT)
	public void postInit(FMLPostInitializationEvent event)
	{
		renderManager = Minecraft.getMinecraft().getRenderManager();
		setEntityList(renderManager);
		this.replaceRender();
	}

	private void replaceRender()
	{
		for(Entry<Class<? extends EntityLivingBase>, RenderLivingBase> key : renderMap.entrySet())
		{
			if (!renderManager.entityRenderMap.containsKey(key.getKey())){
				continue;
			}
			else
			{
				renderManager.entityRenderMap.remove(key.getKey());
				renderManager.entityRenderMap.put(key.getKey(), key.getValue());
			}
		}
	}
}
